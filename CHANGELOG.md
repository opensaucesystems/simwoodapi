# Changelog

All Notable changes to `simwood-api` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [0.2.0] - 2021-03-03

### Added
- Refactored classes and directory structure to be more in-line with standards
- Updated to latest version of `nategood/httpful`
- Added type-hinting to methods
- Added license file
- Added Code of Conduct
- Added docs into the repo
- Added tests for public APIs

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Dropped support for PHP v7.3. If you need to use an older version of PHP, you will need to use the previous version.

### Security
- Nothing

## [0.1.5] - 2017-05-09

### Added
- Add request timeout to default of 10 seconds

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

## [0.1.4] - 2017-01-19

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Fixed deallocate
- Fixed PHPDocs for numbers

### Removed
- Nothing

### Security
- Nothing

## [0.1.3] - 2017-01-19

### Added
- Added 999 to numbers

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

## [0.1.2] - 2017-01-19

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Fixed deallocate

### Removed
- Nothing

### Security
- Nothing

## [0.1.1] - 2015-12-23

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Small changes to the files method, will auto fetch waiting reports

### Removed
- Nothing

### Security
- Nothing

## [0.1.0] - 2015-12-14

### Added
- First stable release

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing
