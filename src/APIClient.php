<?php
namespace opensaucesystems\simwood {

	use \Exception;

	class APIClient {

		private $config;

		public function __construct($apiUser, $apiKey, $data = [])
		{
			$this->config = (object) [
				'baseuri'  => 'https://api.simwood.com/v3/',
				'endpoint' => null,
				'user'     => $apiUser,
				'key'      => $apiKey
			];
		}

		public function __get($endpoint)
		{
			$class = __NAMESPACE__.'\\endpoints\\'.$endpoint;

			if (class_exists($class)) {
				$this->config->endpoint = $endpoint;
				return $this->$endpoint = new $class($this->config);
			} else {
				throw new Exception(
					'Endpoint '.$class.', not implemented.'
				);
			}
		}

	}

}
