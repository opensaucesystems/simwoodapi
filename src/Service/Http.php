<?php

namespace Opensaucesystems\Simwood\Service;

use Httpful\Exception\ConnectionErrorException;
use Httpful\Request;
use TypeError;

trait Http
{
    private function initRequest(): void
    {
        // default request timeout 5 seconds
        $timeout = 5;

        if (isset($this->config['options']['timeout']) && (is_int($this->config['options']['timeout']) || is_float($this->config['options']['timeout']))) {
            $timeout = $this->config['options']['timeout'];
        }

        $template = Request::init()
            ->expectsJson()
            ->timeout($timeout)
            ->authenticateWith(
                $this->config['user'],
                $this->config['key']
            );

        Request::ini($template);
    }

    /**
     *
     * @param string $url
     * @param null|array $options
     * @return array|object
     * @throws TypeError
     * @throws ConnectionErrorException
     */
    public function get(string $url, ?array $options = null)
    {
        $this->initRequest();

        if (isset($options)) {
            $query = http_build_query($options);
            $url .= "?{$query}";
        }

        return Request::get($url)
            ->send()->body;
    }

    /**
     *
     * @param string $url
     * @param null|array $payload
     * @return array|string|object
     * @throws TypeError
     * @throws ConnectionErrorException
     */
    public function put(string $url, ?array $payload = null)
    {
        $this->initRequest();

        return Request::put($url)
            ->body($payload)
            ->send()->body;
    }

    /**
     *
     * @param string $url
     * @param null|array $payload
     * @return array|string|object
     * @throws TypeError
     * @throws ConnectionErrorException
     */
    public function post(string $url, ?array $payload = null)
    {
        $this->initRequest();

        return Request::post($url)
            ->body($payload)
            ->send()->body;
    }

    /**
     *
     * @param string $url
     * @param null|array $payload
     * @return array|string|object
     * @throws TypeError
     * @throws ConnectionErrorException
     */
    public function delete(string $url, ?array $payload = null)
    {
        $this->initRequest();

        return Request::delete($url)
            ->body($payload)
            ->send()->body;
    }

    public function head(): void
    {

    }

    public function options(): void
    {

    }
}
