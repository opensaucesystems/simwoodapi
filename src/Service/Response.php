<?php

namespace Opensaucesystems\Simwood\Service;

trait Response
{
    public function pdf(string $body): void
    {
        header('Content-Type: application/pdf');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . sprintf("%u", strlen($body)));
        exit($body);
    }

    public function json(): void
    {

    }

    public function xml(): void
    {

    }

    public function csv(string $body): void
    {
        header('Content-Type: text/csv');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . sprintf("%u", strlen($body)));
        exit($body);
    }
}
