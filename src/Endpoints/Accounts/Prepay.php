<?php

namespace Opensaucesystems\Simwood\Endpoints\Accounts;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Prepay
{
    use Http, Response;

    /**
     * @psalm-var array{baseuri: string, endpoint: null|string, key: string, options: array, user: string}
     */
    protected array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Accounts - prepay - balance
     *
     * Query account for balance alert value
     * Usage:  $simwood->accounts->prepay->balance($account_id)
     * Result:
     *   Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [balance] => 2.00000
     *               [currency] => GBP
     *           )
     *
     *   )
     *
     * Query account for balance alert value
     * Usage:  $simwood->accounts->prepay->balance($account_id, 'alert', [])
     * Result:
     *  Array
     *  (
     *      [0] => stdClass Object
     *          (
     *              [account] => 930000
     *              [alert_balance] => 0
     *          )
     *
     *  )
     *
     * Update account balance alert (passing 123 as the value to set)
     * Usage:  $simwood->accounts->prepay->balance($account_id, 'alert', ['alert_balance' => 123])
     * Result:
     *   Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [account] => 930000
     *               [alert_balance] => 123
     *           )
     *
     *   )
     *
     * Query account for locked value
     * Usage:  $simwood->accounts->prepay->balance($account_id, 'locked', [])
     * Result:
     *  Array
     *  (
     *      [0] => stdClass Object
     *          (
     *              [account] => 930000
     *              [balance] => 2
     *              [locked] => 0
     *              [available] => 2
     *          )
     *
     *  )
     *
     * Update account locked value (passing 123 as the value to set)
     * Usage:  $simwood->accounts->prepay->balance($account_id, 'locked', ['balance' => 123])
     * Result:
     *  Array
     *  (
     *      [0] => stdClass Object
     *          (
     *              [account] => 930000
     *              [balance] => 2
     *              [locked] => 123
     *              [available] => 2
     *          )
     *
     *  )
     *
     * Delete account locked value (passing 0 as the value to set, which will remove lock)
     * Usage:  $simwood->accounts->prepay->balance($account_id, 'locked', ['balance' => 0])
     * Note: Response will return http 500 if theres no lock in place
     * Result:
     *   Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [account] => 930000
     *               [balance] => 2
     *               [locked] => 0
     *               [available] => 1
     *           )
     *
     *   )
     *
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $action  - Options are alert, locked
     * @param array $data    - Data for delete and put methods
     * @return array
     */
    public function balance(int $account_id, string $action = '', array $data = [])
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/prepay/balance';
        if (!empty($action)) {
            // /alert or /locked
            $endpoint .= '/'.$action;
        }

        // get currently set balance alert
        if (in_array($action, ['alert', 'locked']) && empty($data)) {
            /** @var array */
            return $this->get(
                $this->config['baseuri'].$endpoint
            );
        }

        /**
         * set balance alert - $data = ['alert_balance' => 0]
         */
        if (in_array($action, ['alert', 'locked']) && !empty($data)) {

            if (isset($data['balance']) && is_numeric($data['balance']) && $data['balance'] == 0) {
                //$data['balance'] = (int) $data['balance'];
                /** @var array */
                return $this->delete(
                    $this->config['baseuri'].$endpoint,
                    $data
                );
            }

            /** @var array */
            return $this->put(
                $this->config['baseuri'].$endpoint,
                $data
            );
        }

        // get balance
        /** @var array */
        return $this->get(
            $this->config['baseuri'].$endpoint
        );
    }

    /**
     * Query for prepay summary report
     *
     * Usage:  $simwood->accounts->prepay->summary($account_id);
     *  stdClass Object
     *   (
     *       [mode] =>
     *       [from] => 2015-12-10 00:00:00
     *       [to] => 2015-12-10 23:59:59
     *       [type] => balance_summary
     *       [account] => 930000
     *       [format] => json
     *       [hash] => b07f7b1cff3c187d8df79fc9fe18aabc
     *       [link] => /v3/files/930825/b07f7b1cff3c187d8df79fc9fe18aabc
     *   )
     *
     *  Note: To get the report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *
     *
     * @param int $account_id  - The account id which is provided to you by simwood
     * @param null|string $dateFrom - Options are all, unpaid and paid
     * @param null|string $dateTo   - Data for delete and put methods
     * @return object
     */
    public function summary(int $account_id, ?string $dateFrom = null, ?string $dateTo = null)
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/prepay/summary';

        $data = null;
        if (!empty($dateFrom) && !empty($dateTo)) {
            $data = [
                'from_date' => $dateFrom,
                'to_date' => $dateTo
            ];
        }

        /** @var object */
        return $this->post(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }

    /**
     * Query for prepayments on account [all [, latest [, int]]]
     *
     * Usage:  $simwood->accounts->prepay->prepayments($account_id);
     *         $simwood->accounts->prepay->prepayments($account_id, 'all');
     *         $simwood->accounts->prepay->prepayments($account_id, 'latest', 10);
     *
     *  Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [date] => 2015-11-12
     *               [currency] => GBP
     *               [net] => 2.00
     *               [VAT] => 0.00
     *               [total] => 2.00
     *               [applied_to_account] => 2.00
     *               [invoice_reference] => I3010000000
     *           )
     *
     *   )
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $action  - Options are all, latest
     * @param null|int $subaction  - The number of prepayments to return
     *
     * @return array
     */
    public function prepayments(int $account_id, string $action = 'all', ?int $subaction = null)
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/prepay/prepayments';

        if (!empty($action)) {
            $endpoint .= '/'.$action;
        }

        if (!empty($subaction)) {
            $endpoint .= '/'.$subaction;
        }

        /** @var array */
        return $this->get($this->config['baseuri'].$endpoint);
    }

    /**
     * Query for transfers on account [all [, latest [, int]]]
     *
     * Usage:  $simwood->accounts->prepay->transfers($account_id);
     *         $simwood->accounts->prepay->transfers($account_id, 'all');
     *         $simwood->accounts->prepay->transfers($account_id, 'latest', 10);
     *
     *  Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [date] => 2015-11-12
     *               [currency] => GBP
     *               [net] => 2.00
     *               [VAT] => 0.00
     *               [total] => 2.00
     *               [applied_to_account] => 2.00
     *               [invoice_reference] => I3010000000
     *           )
     *
     *   )
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $action  - Options are all, latest
     * @param null|int $subaction  - The number of transfers to return
     * @return array
     */
    public function transfers(int $account_id, string $action = 'all', ?int $subaction = null)
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/prepay/transfers';

        if (!empty($action)) {
            $endpoint .= '/'.$action;
        }

        if (!empty($subaction)) {
            $endpoint .= '/'.$subaction;
        }

        /** @var array */
        return $this->get($this->config['baseuri'].$endpoint);
    }
}
