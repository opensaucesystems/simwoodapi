<?php

namespace Opensaucesystems\Simwood\Endpoints\Accounts;

use Opensaucesystems\Simwood\Endpoints\BaseEndpoint;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Reports extends BaseEndpoint
{
    use Http, Response;

    public function __construct(array $config)
    {
        parent::__construct($config, __CLASS__);
    }
}
