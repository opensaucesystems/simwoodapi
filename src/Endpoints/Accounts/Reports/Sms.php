<?php

namespace Opensaucesystems\Simwood\Endpoints\Accounts\Reports;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Sms
{
    use Http, Response;

    /**
     * @psalm-var array{baseuri: string, endpoint: null|string, key: string, options: array, user: string}
     */
    protected array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Accounts - report - sms - cdr
     *
     * Query account for sms cdr summary data by day, in or out
     * Usage:  $report = $simwood->accounts->reports->sms->cdr($account_id, 'latest', 10)
     * Result:
     *   Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [account] => 930000
     *               [reference] => 22285b6b71602935605072b3ffc083101ad23123
     *               [date] => 2015-12-10
     *               [time] => 13:27:00
     *               [calldate] => 2015-12-10 13:27:00
     *               [from] => 01234567890
     *               [fromdesc] => NTS
     *               [to] => 441234567890
     *               [todesc] => NTS to VoIP
     *               [toid] => 2523
     *               [secs_call] => 2
     *               [secs_billed] => 2
     *               [secs_peak] => 2
     *               [secs_offpeak] => 0
     *               [secs_weekend] => 0
     *               [chg_peak] => 0
     *               [chg_offpeak] => 0
     *               [chg_weekend] => 0
     *               [chg_connection] => 0
     *               [chg_total] => 0
     *               [currency] => GBP
     *               [trunk] => 930000-L001
     *               [tag] =>
     *           )
     *   )
     *  Note: To get the above report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *
     * @param int $account_id   - The account id which is provided to you by simwood
     * @param string $action    - Options are [day|latest]
     * @param null|int $subaction  - Optional: The number of results to report on [10|100|1000|10000]
     * @param null|string $date      - Optional: mySQL format datetime string
     * @return array
     */
    public function cdr(int $account_id, string $action = 'day', ?int $subaction = null, ?string $date = null)
    {
        $endpoint = 'accounts/'.$account_id.'/reports/sms/cdr/'.$action;

        if (!empty($subaction)) {
            $endpoint .= '/'.$subaction;
        }

        $data = null;
        if (!empty($date)) {
            $data['date'] = $date;
        }

        /** @var array */
        return $this->post(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }
}
