<?php

namespace Opensaucesystems\Simwood\Endpoints\Accounts\Reports;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Admin
{
    use Http, Response;

    /**
     * @psalm-var array{baseuri: string, endpoint: null|string, key: string, options: array, user: string}
     */
    protected array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Accounts - report - admin - cdr
     *
     * Request report of non-call related charges
     * Usage:  $simwood->accounts->reports->admin->cdr($account_id, 'day')
     * Result:
     *    stdClass Object
     *   (
     *       [mode] => chgd
     *       [date] => 2015-12-10
     *       [type] => chg
     *       [account] => 930825
     *       [format] => json
     *       [hash] => 71289698ae810a1a43b4d41f5c67004f
     *       [link] => /v3/files/930825/71289698ae810a1a43b4d41f5c67004f
     *   )
     *
     *   // nothing returned!?, makes sense...
     *   Array
     *   (
     *   )
     *
     *  Note: To get the above report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *
     * @param int $account_id   - The account id which is provided to you by simwood
     * @param string $action    - Options are [day]
     * @param null|string $date      - Optional: mySQL format datetime string
     * @return object
     */
    public function cdr(int $account_id, string $action = 'day', ?string $date = null)
    {
        $endpoint = 'accounts/'.$account_id.'/reports/admin/cdr/'.$action;

        $data = null;
        if (!empty($date)) {
            $data['date'] = $date;
        }

        /** @var object */
        return $this->post(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }
}
