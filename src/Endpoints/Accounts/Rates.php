<?php

namespace Opensaucesystems\Simwood\Endpoints\Accounts;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Rates
{
    use Http, Response;

    /**
     * @psalm-var array{baseuri: string, endpoint: null|string, key: string, options: array, user: string}
     */
    protected array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Request a report of current account termination rates (breakouts)
     *
     * Usage:
     *  $simwood->accounts->rates->breakouts($account_id, 'date', '2015-12-15');
     *
     * Result:
     * stdClass Object
     *   (
     *       [mode] => bod
     *       [date] => 2015-12-11
     *       [type] => rates
     *       [account] => 930000
     *       [format] => json
     *       [hash] => 6668124194c8120cfffee195be2d8000
     *       [link] => /v3/files/930825/6668124194c8120cfffee195be2d8000
     *   )
     *
     *  Note: To get the report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *  Result:
     *  Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [id] => 1
     *               [location_name] => Afghanistan - Proper
     *               [tariff] => 7
     *               [peak_charge] => 0.14080
     *               [offpeak_charge] => 0.14080
     *               [weekend_charge] => 0.14080
     *               [connection_charge] => 0.0000
     *               [basis_minimum] => 1
     *               [basis_increment] => 1
     *               [valid_from] => 2015-11-18 00:00:00
     *               [valid_to] => 2015-12-15 23:59:59
     *               [currency] => GBP
     *               [Type] => Premium
     *               [in_country_order] =>
     *           )
     *
     *       [1] => stdClass Object
     *           (
     *               [id] => 3
     *               [location_name] => Albania - Tirane
     *               [tariff] => 7
     *               [peak_charge] => 0.11630
     *               [offpeak_charge] => 0.11630
     *               [weekend_charge] => 0.11630
     *               [connection_charge] => 0.0000
     *               [basis_minimum] => 1
     *               [basis_increment] => 1
     *               [valid_from] => 2015-10-21 00:00:00
     *               [valid_to] => 2015-12-15 23:59:59
     *               [currency] => GBP
     *               [Type] => Premium
     *               [in_country_order] => 100
     *           )
     *       ...
     *   )
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $action  - Options are [date, future]
     * @param null|string $date    - Optionally specify date in YYYY-MM-DD format to
     *                          request report of historic account termination rates
     * @return object
     */
    public function breakouts(int $account_id, string $action = 'date', ?string $date = null)
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/rates/breakouts';

        // set additional endpoint
        if (!empty($action) && in_array($action, ['date', 'future'])) {
            $endpoint .= '/'.$action;
        } else {
            $endpoint .= '/date';
        }

        // set date param
        $post = null;
        if (!empty($date)) {
            $post['date'] = $date;
        }

        // if action is future clear date
        if ($action == 'future') {
            $post = null;
        }

        /** @var object */
        return $this->post(
            $this->config['baseuri'].$endpoint,
            $post
        );
    }

    /**
     * Request a report of current termination codes (codes)
     *
     * Usage:
     *  $simwood->accounts->rates->codes($account_id, 'date', '2015-12-15');
     *
     * Result:
     * stdClass Object
     *   (
     *       [mode] => cdf
     *       [type] => rates
     *       [account] => 930000
     *       [format] => json
     *       [hash] => ad9f9f39758bf8fc42042eab2c479800
     *       [link] => /v3/files/930825/ad9f9f39758bf8fc42042eab2c479800
     *   )
     *
     *  Note: To get the report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *  Result:
     *  Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [Prefix] => 40787
     *               [id] => 3073
     *               [valid_from] => 2015-12-16 00:00:00
     *               [valid_to] => 0000-00-00 00:00:00
     *           )
     *
     *       [1] => stdClass Object
     *           (
     *               [Prefix] => 40787
     *               [id] => 3076
     *               [valid_from] => 2015-09-01 00:00:00
     *               [valid_to] => 2015-12-15 23:59:59
     *           )
     *       ...
     *   )
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $action  - Options are [date, future]
     * @param null|string $date    - Optionally specify date in YYYY-MM-DD format to
     *                          request report of historic account termination codes
     * @return object
     */
    public function codes(int $account_id, string $action = 'date', ?string $date = null)
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/rates/codes';

        // set additional endpoint
        if (!empty($action) && in_array($action, ['date', 'future'])) {
            $endpoint .= '/'.$action;
        } else {
            $endpoint .= '/date';
        }

        // set date param
        $post = null;
        if (!empty($date)) {
            $post['date'] = $date;
        }

        // if action is future clear date
        if ($action == 'future') {
            $post = null;
        }

        /** @var object */
        return $this->post(
            $this->config['baseuri'].$endpoint,
            $post
        );
    }

    /**
     * Request the latest ratecard in CSV format
     * NB: The options silver, platinum and gold are only for
     *  ‘legacy’ or ‘startup’ account types with multiple rate decks.
     *  Virtual Interconnect and Managed Interconnect customers should
     *  use default.
     *
     * Usage:
     *  $simwood->accounts->rates->csv($account_id, 'gold');
     *
     * Result:
     * #Effective: 2015-12-16
     *   "prefix","location","day","evening","weekend","connection","minimum_sec","increment_sec"
     *   441,"UK - Fixed",0.00249,0.00249,0.00249,0.0000,1,1
     *   442,"UK - Fixed",0.00249,0.00249,0.00249,0.0000,1,1
     *   4474178,"UK - Mobile - Simwood",0.00680,0.00680,0.00680,0.0000,1,1
     *   ...
     *   ...
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $type    - Options are [default|silver|platinum|gold]
     * @return string
     */
    public function csv(int $account_id, string $type = 'default')
    {
        if (!in_array($type, ['default', 'silver', 'platinum', 'gold'])) {
            $type = 'default';
        }

        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/rates/csv/'.$type;

        /** @var string */
        return $this->get(
            $this->config['baseuri'].$endpoint
        );
    }

    /**
     * Destination Lookup
     * Returns the cost of calling the specified number in your account currency.
     * Each rate array is keyed by the relevant deck (1 - platinum, 2 - gold, 3 silver)
     * and shows (p)eak, (o)ffpeak, (w)eekend rates and the (c)onnection charge
     *
     * Usage:
     *  $simwood->accounts->rates->lookup($account_id, '441983123456');
     *
     * Result:
     * stdClass Object
     *   (
     *       [desc] => UK - Fixed
     *       [account_type] => legacy
     *       [currency] => GBP
     *       [rates] => stdClass Object
     *           (
     *               [1] => stdClass Object
     *                   (
     *                       [p] => 0.00249
     *                       [o] => 0.00249
     *                       [w] => 0.00249
     *                       [c] => 0.0000
     *                       [min] => 1
     *                       [inc] => 1
     *                       [ISO] => GB
     *                   )
     *
     *               [2] => stdClass Object
     *                   (
     *                       [p] => 0.00249
     *                       [o] => 0.00249
     *                       [w] => 0.00249
     *                       [c] => 0.0000
     *                       [min] => 1
     *                       [inc] => 1
     *                       [ISO] => GB
     *                   )
     *
     *           )
     *
     *   )
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param string $number  - Phone neumber to lookup, must be international format
     * @return object
     */
    public function lookup(int $account_id, string $number)
    {
        // set endpoint address
        $endpoint = 'accounts/'.$account_id.'/rate/'.$number;

        /** @var object */
        return $this->get(
            $this->config['baseuri'].$endpoint
        );
    }
}
