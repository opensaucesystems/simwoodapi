<?php

namespace Opensaucesystems\Simwood\Endpoints;

use \Exception;

class BaseEndpoint
{
    /**
     * @psalm-var array{baseuri: string, endpoint: null|string, key: string, options: array, user: string}
     */
    protected array $config;

    protected string $endpoint;

    public function __construct(array $config, string $endpoint)
    {
        $this->config = $config;
        $this->endpoint = $endpoint;
    }

    public function __get(string $method)
    {
        $class = $this->endpoint.'\\'.ucfirst($method);

        if (class_exists($class)) {
            $this->config['endpoint'] = $this->endpoint;
            $this->config['method'] = $method;

            return new $class($this->config);
        }

        if (method_exists($this, $method)) {
            return call_user_func([$this, $method], [$this->config]);
        } else {
            throw new Exception('Endpoint '.$class.', not implemented.');
        }
    }
}
