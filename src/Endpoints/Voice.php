<?php

namespace Opensaucesystems\Simwood\Endpoints;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Voice extends BaseEndpoint
{
    use Http, Response;

    public function __construct(array $config)
    {
        parent::__construct($config, __CLASS__);
    }

    /**
     * Voice - CDR
     *
     * Usage:
     *   $simwood->voice->cdr($account_id, [
     *       'date_start' => 'YYYY-MM-DD H:i:s', //date time string
     *       'date_end' => 'YYYY-MM-DD H:i:s',   //date time string
     *       'limit' => '500',                   //int results to be returned
     *       'start' => '',                      //offset for pagination
     *       'filter' => [                       //filtering:
     *           'search_key' => 'value'         // supported keys are from, to, toISO, trunk, tag
     *       ],
     *   ]);
     * Result:
     *   stdClass Object
     *   (
     *       [success] => 1
     *       [count] => 1
     *       [data] => Array
     *           (
     *               [0] => stdClass Object
     *                   (
     *                       [_id] => 22285b6b71602935605072b3ffc083101ad23123
     *                       [time] => 13:27:19
     *                       [date] => 2015-12-10
     *                       [account] => 930825
     *                       [fromdesc] => NTS
     *                       [from] => 01234567890
     *                       [to] => 441234567890
     *                       [toid] => 2523
     *                       [toISO] => GB
     *                       [todesc] => NTS to VoIP
     *                       [calldate] => 2015-12-10 13:27:00
     *                       [secs_call] => 3
     *                       [secs_peak] => 3
     *                       [secs_offpeak] => 0
     *                       [secs_weekend] => 0
     *                       [secs_billed] => 3
     *                       [rate_peak] => 0.00000
     *                       [rate_offpeak] => 0.00000
     *                       [rate_weekend] => 0.00000
     *                       [rate_connection] => 0.0000
     *                       [chg_total] => 0
     *                       [currency] => GBP
     *                       [trunk] => 930000-L001
     *                       [tag] =>
     *                       [reference] => 22285b6b71602935605072b3ffc083101ad23123
     *                   )
     *               ...
     *           )
     *   )
     *
     * @param int $account_id   - The account id which is provided to you by simwood
     * @param array $data       - Filter data
     * @return object
     */
    public function cdr(int $account_id, array $data = [])
    {
        $endpoint = 'voice/'.$account_id.'/cdr';

        if (empty($data)) {
            $data = null;
        }

        /** @var object */
        return $this->post(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }

    /**
     * Voice - cdr record details
     * Retrieve more information, where available, on a specific call
     *  using the reference value returned from the above CDRs (or the CDR Reporting).
     *
     * Usage:
     *   $simwood->voice->cdrDetails($account_id, 'YYYY-MM-DD', '22285b6b71602935605072b3ffc083101ad23123');
     * Result:
     *   stdClass Object
     *   (
     *       [success] =>
     *       [http_code] => 500
     *       [errors] => Array
     *           (
     *               [0] => Temporarily Unavailable
     *           )
     *
     *   )
     *
     * @param int $account_id   - The account id which is provided to you by simwood
     * @param string $date      - YYYY-MM-DD
     * @param string $reference - Hash of the call
     * @return object
     */
    public function cdrDetails(int $account_id, string $date, string $reference)
    {
        $endpoint = 'voice/'.$account_id.'/cdr/'.$date.'/'.$reference;

        /** @var object */
        return $this->get(
            $this->config['baseuri'].$endpoint
        );
    }

    /**
     * Voice - calls in progress
     * Number and value of calls in progress relative to account balance. Useful for fraud monitoring.
     *
     * Usage:
     *   $simwood->voice->inprogress($account_id)
     * Result:
     *   stdClass Object
     *   (
     *       [datetime] => 2015-12-10 16:51:00
     *       [total] => 0
     *       [callcount] => 0
     *       [balance] => 2
     *       [currency] => GBP
     *       [balance_locked] => 0
     *       [balance_available] => 2
     *       [calls] => Array
     *           (
     *           )
     *
     *       [countries] => Array
     *           (
     *           )
     *
     *   )
     *
     * @param int $account_id   - The account id which is provided to you by simwood
     * @return object
     */
    public function inprogress(int $account_id)
    {
        $endpoint = 'voice/'.$account_id.'/inprogress/current';

        /** @var object */
        return $this->get(
            $this->config['baseuri'].$endpoint
        );
    }

    /**
     * Voice - Account Limits (and Dynamic Channel Limits)
     *
     * Dynamic Channel Limits
     *   Any dynamic channel limits shown in the "dynamic" block take precedence
     *   over the usual account limits. e.g. in the above example due to the
     *   low balance (£15) there is a concurrent channel limit of 3 channels
     *   and a rate limit of 3 calls per second.
     *
     * Changing Channel / Rate Limits
     *   These limits are set per-account by Simwood and are based on your
     *   traffic. We do not charge for channels and impose these limits only
     *   to manage our network utilisation.  If you require more channels or
     *   a higher rate of calls per second then please contact us via our
     *   support ticket system.
     *
     * Usage:
     *   $simwood->voice->limits($account_id)
     *
     * Result:
     *   stdClass Object
     *   (
     *       [channel_allocation] => 30
     *       [limit_concurrent_in] =>
     *       [limit_concurrent_out] => 30
     *       [limit_concurrent_out_per_number] =>
     *       [limit_concurrent_out_international] =>
     *       [limit_concurrent_out_hotspot] =>
     *       [limit_rate_out] => 30/10s
     *       [limit_rate_out_international] =>
     *       [limit_rate_out_hotspot] =>
     *       [dynamic] => stdClass Object
     *           (
     *               [balance] => 2
     *               [limit_concurrent_out] => 3
     *               [limit_rate_out] => 3/1s
     *           )
     *
     *   )
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @return object
     */
    public function limits(int $account_id)
    {
        $endpoint = 'voice/'.$account_id.'/limits';

        /** @var stdClass */
        return $this->get(
            $this->config['baseuri'].$endpoint
        );
    }
}
