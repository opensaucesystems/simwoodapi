<?php

namespace Opensaucesystems\Simwood\Endpoints;

use Opensaucesystems\Simwood\Service\Http;
use Opensaucesystems\Simwood\Service\Response;

class Files extends BaseEndpoint
{
    use Http, Response;

    public function __construct(array $config)
    {
        parent::__construct($config, __CLASS__);
    }

    /**
     * Fetch asynchronous report "files"
     *
     * Usage:  $simwood->files->fetch($account_id, $result->hash)
     *
     * @param int $account_id - The account id which is provided to you by simwood
     * @param null|string $hash    - Report hash
     * @return array|string
     */
    public function fetch(int $account_id, ?string $hash = null)
    {
        $endpoint = 'files/'.$account_id;

        if (!empty($hash)) {
            $endpoint .= '/'.$hash;

            /** @var string */
            return $this->get($this->config['baseuri'].$endpoint);
        }

        $result = $this->get($this->config['baseuri'].$endpoint);

        if (!is_object($result)) {
            return [];
        }

        $return = array();
        foreach ($result as $row) {
            if (empty($row->name)) {
                continue;
            }
            $return[] = $this->get($this->config['baseuri'].$endpoint.'/'.$row->name);
        }

        return $return;
    }
}
