<?php

namespace Opensaucesystems\Simwood\Endpoints;

use Opensaucesystems\Simwood\Service\Http;

class Tools extends BaseEndpoint
{
    use Http;

    public function __construct(array $config)
    {
        parent::__construct($config, __CLASS__);
    }

    /**
     * Tools - myip
     *
     * Usage:  $simwood->tools->myip;
     * Result:
     *   stdClass Object
     *   (
     *       [ip] => 123.123.123.123
     *   )
     *
     * @return object
     */
    public function myip()
    {
        /** @var object */
        return $this->get(
            $this->config['baseuri'].'tools/myip'
        );
    }

    /**
     * Tools - time
     *
     * Usage:  $simwood->tools->time;
     * Result:
     *   stdClass Object
     *   (
     *       [timestamp] => 1449690039
     *       [rfc] => Wed, 09 Dec 2015 19:40:39 +0000
     *       [api] => 2015-12-09 19:40:39
     *   )
     *
     * @return object
     */
    public function time()
    {
        /** @var object */
        return $this->get(
            $this->config['baseuri'].'tools/time'
        );
    }
}
