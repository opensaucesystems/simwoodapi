<?php

namespace Opensaucesystems\Simwood\Endpoints;

use Opensaucesystems\Simwood\Service\Http;

class Numbers extends BaseEndpoint
{
    use Http;

    public function __construct(array $config)
    {
        parent::__construct($config, __CLASS__);
    }

    /**
     * Ranges
     *
     * Retrieves a list of all available number ranges, including descriptions.
     * This is intended for customers to populate, for example, a drop down
     * to allow customers to select an area.
     *
     * Usage:  $simwood->numbers->ranges($account_id);
     * Result:
     *   stdClass Object
     *   (
     *       [success] => 1
     *       [data] => Array
     *           (
     *               [0] => stdClass Object
     *                   (
     *                       [id] => 03dd542cafcecf43fc06024ee6099311424c71cf
     *                       [prefix] => 113403
     *                       [sabc] => 1134
     *                       [description] => Geographic - Leeds
     *                       [chargeband] => geo
     *                   )
     *
     *               [1] => stdClass Object
     *                   (
     *                       [id] => c36965d451c1fa708b1c84332af9f52094a16c48
     *                       [prefix] => 114312
     *                       [sabc] => 1143
     *                       [description] => Geographic - Sheffield
     *                       [chargeband] => geo
     *                   )
     *               ...
     *           )
     *   )
     *
     * @param int     $account_id - The account id which is provided to you by simwood
     * @return object
     */
    public function ranges(int $account_id)
    {
        /** @var object */
        return $this->get(
            $this->config['baseuri'].'numbers/'.$account_id.'/ranges'
        );
    }

    /**
     * Available Numbers
     *
     * Returns 1,10 or 100 numbers available for allocation matching
     * the pattern specified.
     *
     * One of all, gold, or standard should be specified in the URL;
     *  all returns all available numbers matching pattern
     *  gold returns only gold numbers matching pattern
     *  standard returns only non-gold numbers matching pattern
     *
     * Usage:  $simwood->numbers->available($account_id, '1983*');
     * Result:
     *   Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [country_code] => 44
     *               [number] => 1983632222
     *               [recommended_gold_premium] => 1550
     *               [wholesale_gold_premium] => 775
     *               [block] => 894a404aa15e7e7353243e31982819ed
     *               [bill_class] => DDI
     *               [SMS] => 0
     *           )
     *
     *       [1] => stdClass Object
     *           (
     *               [country_code] => 44
     *               [number] => 1983632632
     *               [recommended_gold_premium] => 1000
     *               [wholesale_gold_premium] => 500
     *               [block] => 894a404aa15e7e7353243e31982819ed
     *               [bill_class] => DDI
     *               [SMS] => 0
     *           )
     *       ...
     *   )
     *
     * @param int     $account_id - The account id which is provided to you by simwood
     * @param string  $pattern    - Pattern to match * wildcard
     * @param string  $type       - Premium type options are [all|gold|standard]
     * @param int  $count      - 1,10 or 100 numbers available for allocation matching the pattern specified.
     * @return array
     */
    public function available(int $account_id, string $pattern = '', string $type = 'all', int $count = 10)
    {
        /** @var array */
        return $this->post(
            $this->config['baseuri'].'numbers/'.$account_id.'/available/'.$type.'/'.$count,
            ['pattern' => $pattern]
        );
    }

    /**
     * Consecutive
     *
     * Request a report of 10,20,30,40,50,60,70,80,90 or 100 consecutive
     * numbers available for allocation matching the pattern specified.
     *
     * Usage:  $simwood->numbers->consecutive($account_id, '1983*')
     * Result:
     *   stdClass Object
     *   (
     *       [mode] => 10
     *       [pattern] => 1983*
     *       [type] => csec
     *       [account] => 930000
     *       [format] => json
     *       [hash] => 3bf98fedaf1f0fbcea4e305b6e084333
     *       [link] => /v3/files/930825/3bf98fedaf1f0fbcea4e305b6e084333
     *   )
     *
     *  Note: To get the report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *  Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [country_code] => 44
     *               [start] => 1983632000
     *               [end] => 1983632009
     *               [recommended_gold_premium] => 210
     *               [wholesale_gold_premium] => 105.0000
     *               [block] => 894a404aa15e7e7353243e31982819ed
     *               [bill_class] => DDI
     *               [SMS] => 0
     *           )
     *
     *       [1] => stdClass Object
     *           (
     *               [country_code] => 44
     *               [start] => 1983632020
     *               [end] => 1983632029
     *               [recommended_gold_premium] => 1085
     *               [wholesale_gold_premium] => 542.5000
     *               [block] => 894a404aa15e7e7353243e31982819ed
     *               [bill_class] => DDI
     *               [SMS] => 0
     *           )
     *       ...
     *   )
     *
     * @param int     $account_id - The account id which is provided to you by simwood
     * @param string  $pattern    - Pattern to match * wildcard
     * @param int  $count      - 1,10 or 100 numbers available for allocation matching the pattern specified.
     * @return object
     */
    public function consecutive(int $account_id, string $pattern = '', int $count = 10)
    {
        /** @var object */
        return $this->post(
            $this->config['baseuri'].'numbers/'.$account_id.'/available/consecutive/'.$count,
            ['pattern' => $pattern]
        );
    }

    /**
     * Allocated
     *
     * Request a report of all current allocated numbers on account.
     *
     * count  : Request a report of the first [ 10 | 100 | 1,000 | 10,000 ]
     *          numbers that match the optional pattern.
     * pattern: Optionally specify to include only those numbers that match
     *          the specified pattern. (can use wildcards e.g *203*).
     * key:     Only return those numbers that match the specified key
     *          in their metadata (see Advanced Routing below)
     *          NB Keys are case-insensitive, wildcards not supported.
     *
     * Usage:
     *  $report = $simwood->numbers->allocated($account_id, [
     *       'count' => 10,
     *       'pattern' => '1983*',
     *       'key' => null,
     *   ]);
     *
     * Result:
     *   stdClass Object
     *   (
     *       [quantity] => 10
     *       [mode] => nlsn
     *       [pattern] => 1983*
     *       [type] => nlist
     *       [account] => 930000
     *       [format] => json
     *       [hash] => 072dfa11ceded69978711888030f8000
     *       [link] => /v3/files/930825/072dfa11ceded69978711888030f8000
     *   )
     *
     *  Note: To get the report data you must call the files endpoint:
     *  $simwood->files->fetch($report->account, $report->hash)
     *  Array
     *   (
     *       [0] => stdClass Object
     *           (
     *               [country_code] => 44
     *               [number] => 1983632140
     *               [gold_price] => 0
     *               [block] => 894a404aa15e7e7353243e31982819ed
     *               [type] => DDI
     *               [SMS] => 0
     *           )
     *
     *   )
     *
     * @param int    $account_id - The account id which is provided to you by simwood
     * @param array  $data       - Configuration request array
     * @return object
     */
    public function allocated(int $account_id, array $data = [])
    {
        $endpoint = $this->config['baseuri'].'numbers/'.$account_id.'/allocated';

        $post = null;
        if (!empty($data)) {
            if (!empty($data['count']) && is_numeric($data['count'])) {
                $endpoint .= '/'.$data['count'];
            } else {
                $endpoint .= '/all';
            }
            $post = [
                'pattern' => $data['pattern'],
                'key' => $data['key'],
            ];
        } else {
            $endpoint .= '/all';
        }

        /** @var object */
        return $this->post(
            $endpoint,
            $post
        );
    }

    /**
     * Number Routing Configuration - Advanced
     *
     * Introduced in May 2014, the following is the preferred way of
     * configuring a number, the previous method is still detailed in this
     * document but is deprecated and will be removed in a future revision
     * of the API. When a new configuration is provided this will take
     * precedence over any existing configuration.
     *
     * Usage:
     *  $simwood->numbers->config($account_id, '44198123456');
     *
     * Result:
     *   stdClass Object
     *   (
     *       [rules] => Array
     *           (
     *           )
     *
     *       [routing] => stdClass Object
     *           (
     *               [default] => Array
     *                   (
     *                       [0] => Array
     *                           (
     *                               [0] => stdClass Object
     *                                   (
     *                                       [type] => sip
     *                                       [endpoint] => 0198123456@voip.example.com
     *                                   )
     *
     *                           )
     *
     *                   )
     *
     *           )
     *
     *       [options] => stdClass Object
     *           (
     *               [enabled] => 1
     *               [block_payphone] => 1
     *               [acr] =>
     *               [icr] =>
     *           )
     *
     *       [meta] => stdClass Object
     *           (
     *               [key] => value
     *           )
     *
     *   )
     *
     * @param int    $account_id - The account id which is provided to you by simwood
     * @param string $number     - Number to configure
     * @param null|array  $data  - Data for replacing active configuration for number.
     *                             This must be an array not json, see official docs
     *                             for structure and options.
     * @return object
     */
    public function config(int $account_id, string $number, ?array $data = null)
    {
        $endpoint = 'numbers/'.$account_id.'/allocated/'.$number.'/config';

        if (empty($data)) {
            /** @var object */
            return $this->get($this->config['baseuri'].$endpoint);
        }

        /** @var object */
        return $this->put(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }

    /**
     * Default Number Routing Configuration - Advanced
     *
     * Similarly, it is now possible to set a default configuration which
     * will be used for all numbers on your account where no other
     * configuration exists - this is ideal for customers who send all calls
     * to a SIP URI and handle onward routing themselves
     *
     * Usage:
     *  $simwood->numbers->defaults($account_id [, config array]);
     *
     * Result:
     *   ...
     * @param int    $account_id - The account id which is provided to you by simwood
     * @param null|array  $data  - Data for replacing active configuration for number.
     *                             This must be an array not json, see official docs
     *                             for structure and options.
     * @return object
     */
    public function defaults(int $account_id, ?array $data = null)
    {
        $endpoint = 'numbers/'.$account_id.'/default/config';

        if (empty($data)) {
            /** @var object */
            return $this->get($this->config['baseuri'].$endpoint);
        }

        /** @var object */
        return $this->put(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }

    /**
     * Number Configuration - 999 Emergency Services
     *
     * Fetch or update emergency services details,
     * You must do your own logic to check whether fields are required or not.
     *
     * For Individual End Users: There title and forname, including there address is required
     * Business End Users: The company name, suffix and there address is required
     *
     *   Usage (Individual End User):
     *   $simwood->numbers->emergency_services($account_id, $number, [
     *       'title' => 'Mr',
     *       'forename' => 'Lawrence',
     *       'name' => 'Cherone',
     *       'premises' => '123',              // (e.g. 104, The Lighthouse, Thatched Cottage)
     *       'thoroughfare' => 'Street name',  // (e.g. King Street, Station Road, Beech Avenue)
     *       'locality' => 'Some town',        // Village or an area within an Town and Town if possible
     *       'postcode' => 'PO11 1AB'          // The full current postcode as recognised by Royal Mail's -
     *                                           //  PAF database. This must be in the form Out-code space In -
     *                                           //  code e.g. LS11 5DF, S9 5AD, S60 3ML
     *   ]);
     *
     *   Usage (Business End User):
     *   $simwood->numbers->emergency_services($account_id, $number, [
     *       'name' => 'Company4U',            // Business names should be chosen that best allow the
     *                                           //  Emergency Services to identify and locate the business
     *                                           //  typically this is the "name over the door" rather than
     *                                           //  that of a parent or holding company irrespective of who
     *                                           //  you address the bill to.
     *
     *       'bussuffix' => 'Ltd'              // Addition to business name (e.g. Ltd or Plc) this can also
     *                                           //  be used to include a brief description that identifies
     *                                           //  the function of the business
     *                                           //  - e.g. "Hospital", "Hotel", "Fuel Storage Depot" provides
     *                                           //  valuable extra information to the Emergency Services.
     *
     *       'premises' => '123',              // (e.g. 104, The Lighthouse, Thatched Cottage)
     *       'thoroughfare' => 'Street name',  // (e.g. King Street, Station Road, Beech Avenue)
     *       'locality' => 'Some town',        // Village or an area within an Town and Town if possible
     *       'postcode' => 'PO11 1AB'          // The full current postcode as recognised by Royal Mail's
     *                                           //  PAF database. This must be in the form Out-code space In
     *                                           //  code e.g. LS11 5DF, S9 5AD, S60 3ML
     *   ]);
     *
     *
     * @param int    $account_id - The account id which is provided to you by simwood
     * @param string $number     - Number to query/configure
     * @param array  $data       - Data for replacing active configuration for number.
     *                             This must be an array not json, see official docs
     *                             for structure and options.
     * @return array|object
     */
    public function emergency_services(int $account_id, string $number, array $data = [])
    {
        $endpoint = 'numbers/'.$account_id.'/allocated/'.$number.'/999';

        if (empty($data)) {
            /** @var object */
            return $this->get($this->config['baseuri'].$endpoint);
        }

        // basic checks for safety on address fields
        $errors = [];

        //
        if (empty($data['premises'])) {
            $errors['premises'] = 'Premises is required for emergency services.';
        }

        //
        if (empty($data['thoroughfare'])) {
            $errors['thoroughfare'] = 'Thoroughfare/Street is required for emergency services.';
        }

        //
        if (empty($data['locality'])) {
            $errors['locality'] = 'Locality is required for emergency services.';
        }

        //
        if (empty($data['postcode'])) {
            $errors['postcode'] = 'Postcode is required for emergency services.';
        }


        if (!empty($errors)) {
            return ['errors' => $errors];
        }

        /** @var object */
        return $this->put(
            $this->config['baseuri'].$endpoint,
            $data
        );
    }
}
