<?php
namespace opensaucesystems\simwood\endpoints\voice {

    class channels {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            $this->config = $config;
        }

        /**
         * Current channel utilisation
         *  NB: this is returned as an array (with one member) for compatibility
         *  with the /channels/history function detailed below
         *
         * Usage:
         *  $simwood->voice->channels->current($account_id);
         *
         * Result:
         *  Array
            (
                [0] => stdClass Object
                    (
                        [datetime] => 2015-12-12 23:23:23
                        [channels] => 0
                        [channels_out] => 0
                        [channels_in] => 0
                    )

            )
         * @param int $account_id - The account id which is provided to you by simwood
         * @return array
         */
        public function current($account_id)
        {
            // set endpoint address
            $endpoint = 'voice/'.$account_id.'/channels/current';

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

        /**
         * Recent (around 24h) channel utilisation samples
         * The  channels count shows the peak number of channels in use between
         * the previous datetime timestamp and the current one.
         *
         * Usage:
         *  $simwood->voice->channels->history($account_id, '1h')
         *
         * Result:
         *  Array
            (
                [0] => stdClass Object
                    (
                        [datetime] => 2015-12-12 23:34:27
                        [channels] => 0
                    )

            )
         * @param int $account_id - The account id which is provided to you by simwood
         * @param int $interval   - Optional interval for samples in the following form [1m, 5m, 1h]
         * @return array
         */
        public function history($account_id, $interval = '5m')
        {
            // set endpoint address
            $endpoint = 'voice/'.$account_id.'/channels/history';

            return $this->get(
                $this->config->baseuri.$endpoint,
                ['interval' => $interval]
            );
        }

    }

}
