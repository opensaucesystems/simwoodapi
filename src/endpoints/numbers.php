<?php
namespace opensaucesystems\simwood\endpoints {

    class numbers extends baseEndpoint {

        use \opensaucesystems\simwood\service\http;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Ranges
         *
         * Retrieves a list of all available number ranges, including descriptions.
         * This is intended for customers to populate, for example, a drop down
         * to allow customers to select an area.
         *
         * Usage:  $simwood->numbers->ranges($account_id);
         * Result:
            stdClass Object
            (
                [success] => 1
                [data] => Array
                    (
                        [0] => stdClass Object
                            (
                                [id] => 03dd542cafcecf43fc06024ee6099311424c71cf
                                [prefix] => 113403
                                [sabc] => 1134
                                [description] => Geographic - Leeds
                                [chargeband] => geo
                            )

                        [1] => stdClass Object
                            (
                                [id] => c36965d451c1fa708b1c84332af9f52094a16c48
                                [prefix] => 114312
                                [sabc] => 1143
                                [description] => Geographic - Sheffield
                                [chargeband] => geo
                            )
                        ...
                    )
            )

         * @param int     $account_id - The account id which is provided to you by simwood
         * @return array
         */
        public function ranges($account_id)
        {
            return $this->get(
                $this->config->baseuri.'numbers/'.$account_id.'/ranges'
            );
        }

        /**
         * Available Numbers
         *
         * Returns 1,10 or 100 numbers available for allocation matching
         * the pattern specified.
         *
         * One of all, gold, or standard should be specified in the URL;
         *  all returns all available numbers matching pattern
         *  gold returns only gold numbers matching pattern
         *  standard returns only non-gold numbers matching pattern
         *
         * Usage:  $simwood->numbers->available($account_id, '1983*');
         * Result:
            Array
            (
                [0] => stdClass Object
                    (
                        [country_code] => 44
                        [number] => 1983632222
                        [recommended_gold_premium] => 1550
                        [wholesale_gold_premium] => 775
                        [block] => 894a404aa15e7e7353243e31982819ed
                        [bill_class] => DDI
                        [SMS] => 0
                    )

                [1] => stdClass Object
                    (
                        [country_code] => 44
                        [number] => 1983632632
                        [recommended_gold_premium] => 1000
                        [wholesale_gold_premium] => 500
                        [block] => 894a404aa15e7e7353243e31982819ed
                        [bill_class] => DDI
                        [SMS] => 0
                    )
                ...
            )

         * @param int     $account_id - The account id which is provided to you by simwood
         * @param string  $pattern    - Pattern to match * wildcard
         * @param string  $type       - Premium type options are [all|gold|standard]
         * @param string  $count      - 1,10 or 100 numbers available for allocation matching the pattern specified.
         * @return array
         */
        public function available($account_id, $pattern = '', $type = 'all', $count = 10)
        {
            return $this->post(
                $this->config->baseuri.'numbers/'.$account_id.'/available/'.$type.'/'.$count,
                ['pattern' => $pattern]
            );
        }

        /**
         * Consecutive
         *
         * Request a report of 10,20,30,40,50,60,70,80,90 or 100 consecutive
         * numbers available for allocation matching the pattern specified.
         *
         * Usage:  $simwood->numbers->consecutive($account_id, '1983*')
         * Result:
            stdClass Object
            (
                [mode] => 10
                [pattern] => 1983*
                [type] => csec
                [account] => 930000
                [format] => json
                [hash] => 3bf98fedaf1f0fbcea4e305b6e084333
                [link] => /v3/files/930825/3bf98fedaf1f0fbcea4e305b6e084333
            )

         *  Note: To get the report data you must call the files endpoint:
         *  $simwood->files->fetch($report->account, $report->hash)
         *  Array
            (
                [0] => stdClass Object
                    (
                        [country_code] => 44
                        [start] => 1983632000
                        [end] => 1983632009
                        [recommended_gold_premium] => 210
                        [wholesale_gold_premium] => 105.0000
                        [block] => 894a404aa15e7e7353243e31982819ed
                        [bill_class] => DDI
                        [SMS] => 0
                    )

                [1] => stdClass Object
                    (
                        [country_code] => 44
                        [start] => 1983632020
                        [end] => 1983632029
                        [recommended_gold_premium] => 1085
                        [wholesale_gold_premium] => 542.5000
                        [block] => 894a404aa15e7e7353243e31982819ed
                        [bill_class] => DDI
                        [SMS] => 0
                    )
                ...
            )

         * @param int     $account_id - The account id which is provided to you by simwood
         * @param string  $pattern    - Pattern to match * wildcard
         * @param string  $count      - 1,10 or 100 numbers available for allocation matching the pattern specified.
         * @return array
         */
        public function consecutive($account_id, $pattern = '', $count = 10)
        {
            return $this->post(
                $this->config->baseuri.'numbers/'.$account_id.'/available/consecutive/'.$count,
                ['pattern' => $pattern]
            );
        }

        /**
         * Allocated
         *
         * Request a report of all current allocated numbers on account.
         *
         * count  : Request a report of the first [ 10 | 100 | 1,000 | 10,000 ]
         *          numbers that match the optional pattern.
         * pattern: Optionally specify to include only those numbers that match
         *          the specified pattern. (can use wildcards e.g *203*).
         * key:     Only return those numbers that match the specified key
         *          in their metadata (see Advanced Routing below)
         *          NB Keys are case-insensitive, wildcards not supported.
         *
         * Usage:
         *  $report = $simwood->numbers->allocated($account_id, [
                'count' => 10,
                'pattern' => '1983*',
                'key' => null,
            ]);
         *
         * Result:
            stdClass Object
            (
                [quantity] => 10
                [mode] => nlsn
                [pattern] => 1983*
                [type] => nlist
                [account] => 930000
                [format] => json
                [hash] => 072dfa11ceded69978711888030f8000
                [link] => /v3/files/930825/072dfa11ceded69978711888030f8000
            )

         *  Note: To get the report data you must call the files endpoint:
         *  $simwood->files->fetch($report->account, $report->hash)
         *  Array
            (
                [0] => stdClass Object
                    (
                        [country_code] => 44
                        [number] => 1983632140
                        [gold_price] => 0
                        [block] => 894a404aa15e7e7353243e31982819ed
                        [type] => DDI
                        [SMS] => 0
                    )

            )

         * @param int    $account_id - The account id which is provided to you by simwood
         * @param array  $data       - Configuration request array
         * @return array
         */
        public function allocated($account_id, $data = [])
        {
            $endpoint = $this->config->baseuri.'numbers/'.$account_id.'/allocated';

            $post = null;
            if (!empty($data)) {
                if (!empty($data['count']) && is_numeric($data['count'])) {
                    $endpoint .= '/'.$data['count'];
                } else {
                    $endpoint .= '/all';
                }
                $post = [
                    'pattern' => $data['pattern'],
                    'key' => $data['key'],
                ];
            } else {
                $endpoint .= '/all';
            }
            return $this->post(
                $endpoint,
                $post
            );
        }

        /**
         * Number Routing Configuration - Advanced
         *
         * Introduced in May 2014, the following is the preferred way of
         * configuring a number, the previous method is still detailed in this
         * document but is deprecated and will be removed in a future revision
         * of the API. When a new configuration is provided this will take
         * precedence over any existing configuration.
         *
         * Usage:
         *  $simwood->numbers->config($account_id, '44198123456');
         *
         * Result:
            stdClass Object
            (
                [rules] => Array
                    (
                    )

                [routing] => stdClass Object
                    (
                        [default] => Array
                            (
                                [0] => Array
                                    (
                                        [0] => stdClass Object
                                            (
                                                [type] => sip
                                                [endpoint] => 0198123456@voip.example.com
                                            )

                                    )

                            )

                    )

                [options] => stdClass Object
                    (
                        [enabled] => 1
                        [block_payphone] => 1
                        [acr] =>
                        [icr] =>
                    )

                [meta] => stdClass Object
                    (
                        [key] => value
                    )

            )

         * @param int    $account_id - The account id which is provided to you by simwood
         * @param string $number     - Number to configure
         * @param array  $data       - Data for replacing active configuration for number.
         *                             This must be an array not json, see official docs
         *                             for structure and options.
         * @return array
         */
        public function config($account_id, $number, $data = null)
        {
            $endpoint = 'numbers/'.$account_id.'/allocated/'.$number.'/config';

            if (empty($data)) {
                return $this->get($this->config->baseuri.$endpoint);
            }

            return $this->put(
                $this->config->baseuri.$endpoint,
                $data
            );
        }

        /**
         * Default Number Routing Configuration - Advanced
         *
         * Similarly, it is now possible to set a default configuration which
         * will be used for all numbers on your account where no other
         * configuration exists - this is ideal for customers who send all calls
         * to a SIP URI and handle onward routing themselves
         *
         * Usage:
         *  $simwood->numbers->defaults($account_id [, config array]);
         *
         * Result:
            ...
         * @param int    $account_id - The account id which is provided to you by simwood
         * @param array  $data       - Data for replacing active configuration for number.
         *                             This must be an array not json, see official docs
         *                             for structure and options.
         * @return array
         */
        public function defaults($account_id, $data = null)
        {
            $endpoint = 'numbers/'.$account_id.'/default/config';

            if (empty($data)) {
                return $this->get($this->config->baseuri.$endpoint);
            }

            return $this->put(
                $this->config->baseuri.$endpoint,
                $data
            );
        }

    }

}
