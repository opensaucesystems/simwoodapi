<?php
namespace opensaucesystems\simwood\endpoints {

    class tools extends baseEndpoint {

        use \opensaucesystems\simwood\service\http;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Tools - myip
         *
         * Usage:  $simwood->tools->myip;
         * Result:
            stdClass Object
            (
                [ip] => 123.123.123.123
            )
         */
        public function myip()
        {
            return $this->get(
                $this->config->baseuri.'tools/myip'
            );
        }

        /**
         * Tools - time
         *
         * Usage:  $simwood->tools->time;
         * Result:
            stdClass Object
            (
                [timestamp] => 1449690039
                [rfc] => Wed, 09 Dec 2015 19:40:39 +0000
                [api] => 2015-12-09 19:40:39
            )
         */
        public function time()
        {
            return $this->get(
                $this->config->baseuri.'tools/time'
            );
        }

    }

}
