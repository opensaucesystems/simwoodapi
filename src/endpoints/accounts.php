<?php
namespace opensaucesystems\simwood\endpoints {

    class accounts extends baseEndpoint {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Accounts - credit
         *
         * Usage:  $simwood->accounts->credit($account_id, 'invoices', 'all|unpaid|paid')
         * Result:
            stdClass Object
            (
                [Invoices] => Array
                    (
                        [0] => stdClass Object
                            (
                                [InvoiceNumber] => I3010008404
                                [Date] => 2015-11-12
                                [DueDate] => 2015-11-12
                                [Currency] => GBP
                                [Net] => 2.00
                                [VAT] => 0.00
                                [Total] => 2.00
                                [AmountPaid] => 2.00
                                [AmountCredited] => 0.00
                                [AmountDue] => 0.00
                            )

                    )

                [TotalDue] => stdClass Object
                    (
                        [GBP] => 0
                    )

                [TotalOverDue] =>
            )
         *
         * @param int $account_id   - The account id which is provided to you by simwood
         * @param string $action    - Default is invoice but the API may add others
         * @param string $subaction - Options are all, unpaid and paid.
         * @return object
         */
        public function credit($account_id, $action = 'invoices', $subaction = 'all')
        {
            $endpoint = 'accounts/'.$account_id.'/credit/'.$action.'/'.$subaction;

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

        /**
         * Accounts - invoice
         *
         * Generates an account invoice PDF string or view in browser.
         *
         * Usage:
         *  $simwood->accounts->invoice($account_id, 'I3000000000', true)
         *
         * @param int $account_id       - The account id which is provided to you by simwood
         * @param string $InvoiceNumber - @see $this->credit() response
         * @param bool $sent            - Add HTTP headers for view in browser
         *
         * @return string
         */
        public function invoice($account_id, $InvoiceNumber, $send = false)
        {
            $return = $this->credit($account_id, 'invoices', $InvoiceNumber);

            if ($send) {
                $this->pdf($return);
            }

            return $return;
        }

        /**
         * Accounts - summary [BETA]
         *
         * The new Summary Reports are not asynchronous, the response is inline.
         * NB These are intended to be indicative only and are not, at this time, suitable for billing purposes
         *
         * Possible usage:  $simwood->accounts->summary($account_id, 'destid|iso|codec|tag|trunk', 'in|out', [
         *      'date_start' => 'YYYY-MM-DD H:i:s',
         *      'date_end' => 'YYYY-MM-DD H:i:s',
         *      'limit' => 9999,
         *      'sort' => 'trunk',
         *      'filter' => array('search_key' => 'value'),
         * ])
         *
         * Response example: $simwood->accounts->summary($account_id, 'trunk')
         * Array
            (
                [0] => stdClass Object
                    (
                        [trunk] => 930000-l001
                        [calls] => 2
                        [acd] => 0.08
                        [minutes] => 0.17
                        [charges] => 0
                    )

            )

         * @param int $account_id   - The account id which is provided to you by simwood
         * @param string $key       - destid|iso|codec|tag|trunk
         * @param string $direction - in|out
         * @param array $data       - Report filtering array
         *
         * @return array
         */
        public function summary($account_id, $key, $direction = '', $data = [])
        {
            // set endpoint address
            $endpoint = 'accounts/'.$account_id.'/summary';

            if (!empty($direction)) {
               $endpoint .= '/'.$direction;
            }

            if (!empty($key)) {
               $endpoint .= '/'.$key;
            }

            if (empty($data)) {
                $data = null;
            }

            return $this->post(
                $this->config->baseuri.$endpoint,
                $data
            );
        }

        /**
         * Destination Lookup
         * Returns the cost of calling the specified number in your account currency.
         * Each rate array is keyed by the relevant deck (1 - platinum, 2 - gold, 3 silver)
         * and shows (p)eak, (o)ffpeak, (w)eekend rates and the (c)onnection charge
         *
         * Usage:
         *  $simwood->accounts->rate($account_id, '441983123456');
         *  or via the rates method
         *  $simwood->accounts->rates->lookup($account_id, '441983123456');
         *
         * Result:
         * stdClass Object
            (
                [desc] => UK - Fixed
                [account_type] => legacy
                [currency] => GBP
                [rates] => stdClass Object
                    (
                        [1] => stdClass Object
                            (
                                [p] => 0.00249
                                [o] => 0.00249
                                [w] => 0.00249
                                [c] => 0.0000
                                [min] => 1
                                [inc] => 1
                                [ISO] => GB
                            )

                        [2] => stdClass Object
                            (
                                [p] => 0.00249
                                [o] => 0.00249
                                [w] => 0.00249
                                [c] => 0.0000
                                [min] => 1
                                [inc] => 1
                                [ISO] => GB
                            )

                    )

            )

         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $number  - Phone neumber to lookup, must be international format
         * @return object
         */
        public function rate($account_id, $number)
        {
            // set endpoint address
            $endpoint = 'accounts/'.$account_id.'/rate/'.$number;

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

        /**
         * Event Notifications
         * Handles all the calls to the notifications endpoint.
         *
         * NB: Think I've packed to much into this method, so it may get changed in future.
         *
         *  List active notifications on your account:
         *   $simwood->accounts->notifications($account_id)
         *
         *  List available notification TYPEs
         *   $simwood->accounts->notifications($account_id, 'available')
         *
         *  List configured recipients for notifications of {TYPE}
         *   $simwood->accounts->notifications($account_id, 'blocked_calls', 'get')
         *
         *  Delete all configured notifications of {TYPE}
         *   $simwood->accounts->notifications($account_id, 'blocked_calls', 'delete')
         *
         *  Shows all configured recipients of notifications of {TYPE} using {METHOD}
         *   $simwood->accounts->notifications($account_id, 'blocked_calls', 'email')
         *
         *  Add a new notification recipient to receive notifications of {TYPE} using
         *  {METHOD}. Returns a hash corresponding to this recipient
         *   $simwood->accounts->notifications($account_id, 'blocked_calls', 'email', 'new destination')
         *
         *  Shows the information on this recipient
         *   $simwood->accounts->notifications($account_id, 'blocked_calls', 'email', '{HASH}', 'get')
         *
         *  Deletes this recipient
         *   $simwood->accounts->notifications($account_id, 'blocked_calls', 'email', '{HASH}', 'delete')
         *
         *  Retrieve a history of recent (last 60 days) notifications on your account
         *   All parameters below are optional, by default will return all notifications for the last 60 days:
         *   class:      Class of notification ( e.g. trunk or billing )
         *   date_start: Start date (no more than 60 days ago)
         *   date_end:   End date (YYYY-MM-DD H:i:s)
         *
         *  Examples:
         *   $simwood->accounts->notifications($account_id, 'history')
         *   $simwood->accounts->notifications($account_id, 'history', 'class')
         *   $simwood->accounts->notifications($account_id, 'history', 'class', 'date_start', 'date_end')
         *
         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
         * @param string $method  - Type of notification method [email, http, sms]
         * @param string $hash    - The {HASH} referred to above can be generated
         *                          locally and is simply an md5()’d version of the notification address
         * @param string $do      - Options are get or delete
         * @return object
         */
        public function notifications($account_id, $type = null, $method = null, $hash = null, $do = null)
        {
            // set endpoint address
            $endpoint = 'accounts/'.$account_id.'/notifications';

            // $simwood->accounts->notifications($account_id)
            if ($type == null && $method == null && $hash == null && empty($data)) {
                return $this->get(
                    $this->config->baseuri.$endpoint
                );
            }

            // $simwood->accounts->notifications($account_id, 'available')
            if ($type == 'available' && $method == null && $hash == null && empty($data)) {
                return $this->get(
                    $this->config->baseuri.$endpoint.'/'.$type
                );
            }

            // $simwood->accounts->notifications($account_id, 'history')
            // $simwood->accounts->notifications($account_id, 'history', 'class')
            // $simwood->accounts->notifications($account_id, 'history', 'class', 'date_start', 'date_end')
            if ($type == 'history') {
                $params = null;

                if (!empty($method)) {
                   $params['class'] = $method;
                }

                if (!empty($hash)) {
                   $params['date_start'] = $hash;
                }

                if (!empty($do)) {
                   $params['date_end'] = $do;
                }

                return $this->get(
                    $this->config->baseuri.$endpoint.'/'.$type,
                    $params
                );
            }

            // $simwood->accounts->notifications($account_id, 'blocked_calls', 'get')
            if (!empty($type) && $method == 'get' && $hash == null && empty($data)) {
                return $this->get(
                    $this->config->baseuri.$endpoint.'/'.$type
                );
            }

            // $simwood->accounts->notifications($account_id, 'blocked_calls', 'delete')
            if (!empty($type) && $method == 'delete' && $hash == null && empty($data)) {
                return $this->delete(
                    $this->config->baseuri.$endpoint.'/'.$type
                );
            }

            // $simwood->accounts->notifications($account_id, 'blocked_calls', 'email')
            if (!empty($type) && in_array($method, ['email', 'http', 'sms']) && $hash == null && empty($data)) {
                return $this->get(
                    $this->config->baseuri.$endpoint.'/'.$type.'/'.$method
                );
            }

            // $simwood->accounts->notifications($account_id, 'blocked_calls', 'email', 'new destination')
            if (!empty($type) && in_array($method, ['email', 'http', 'sms']) && !empty($hash) && empty($data)) {
                $post = [
                    'destination' => $hash
                ];
                return $this->post(
                    $this->config->baseuri.$endpoint.'/'.$type.'/'.$method,
                    $post
                );
            }

            // $simwood->accounts->notifications($account_id, 'blocked_calls', 'email', 'hash', 'get')
            if (!empty($type) && in_array($method, ['email', 'http', 'sms']) && !empty($hash) && $do == 'get') {
                return $this->get(
                    $this->config->baseuri.$endpoint.'/'.$type.'/'.$method.'/'.$hash
                );
            }

            // $simwood->accounts->notifications($account_id, 'blocked_calls', 'email', 'hash', 'delete')
            if (!empty($type) && in_array($method, ['email', 'http', 'sms']) && !empty($hash) && $do == 'delete') {
                return $this->delete(
                    $this->config->baseuri.$endpoint.'/'.$type.'/'.$method.'/'.$hash
                );
            }

            throw new Exception('Your doing it wrong, check manual');
        }

    }

}
