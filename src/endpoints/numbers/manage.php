<?php
namespace opensaucesystems\simwood\endpoints\numbers {

    class manage {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            $this->config = $config;
        }

        /**
         * Return configuration information on allocated number
         *
         * Usage:
         *  $simwood->numbers->manage->info($account_id, '44198123456')
         *
         * Result:
         *  stdClass Object
            (
                [basic] => stdClass Object
                    (
                        [link] => /v3/numbers/930000/allocated/44198123456/basic
                        [country_code] => 44
                        [number] => 1983123456
                        [recommended_gold_premium] =>
                        [wholesale_gold_premium] => 0.0000
                        [block] => 894a404aa15e7e7353243e3198281333
                        [bill_class] => DDI
                        [SMS] => 0
                    )

                [voice] => stdClass Object
                    (
                        [link] => /v3/numbers/930000/allocated/44198123456/voice
                        [app] => Custom
                        [appdata] => Advanced Routing
                    )

                [fax] => stdClass Object
                    (
                        [link] => /v3/numbers/930000/allocated/44198123456/fax
                    )

                [sms] => stdClass Object
                    (
                        [link] => /v3/numbers/930000/allocated/44198123456/sms
                    )

                [999] => stdClass Object
                    (
                        [link] => /v3/numbers/930000/allocated/44198123456/999
                    )

            )
         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $number  - Phone number on account
         * @return object
         */
        public function info($account_id, $number)
        {
            // set endpoint address
            $endpoint = 'numbers/'.$account_id.'/allocated/'.$number;

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

        /**
         * Allocate an available number to the account
         *
         * Usage:
         *  $simwood->numbers->manage->allocate($account_id, '44198123456')
         *
         * Result:
         * ...
         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $number  - Phone number on account
         * @return object
         */
        public function allocate($account_id, $number)
        {
            // set endpoint address
            $endpoint = 'numbers/'.$account_id.'/allocated/'.$number;

            return $this->put(
                $this->config->baseuri.$endpoint
            );
        }

        /**
         * De-configure and irrevocably remove number from account
         *
         * Usage:
         *  $simwood->numbers->manage->deallocate($account_id, '44198123456')
         *
         * Result:
         * ...
         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $number  - Phone number on account
         * @return object
         */
        public function deallocate($account_id, $number)
        {
            // set endpoint address
            $endpoint = 'numbers/'.$account_id.'/allocated/'.$number;

            return $this->delete(
                $this->config->baseuri.$endpoint
            );
        }

        /**
         * Alias of deallocate
         * Usage:
         *  $simwood->numbers->manage->delete($account_id, '44198123456')
         */
        public function delete($account_id, $number) {
            return $this->deallocate($account_id, $number);
        }

        /**
         * Alias of deallocate
         * Usage:
         *  $simwood->numbers->manage->remove($account_id, '44198123456')
         */
        public function remove($account_id, $number) {
            return $this->deallocate($account_id, $number);
        }

        /**
         * Object describing the most recent call to this number
         *
         * Usage:
         *  $simwood->numbers->manage->lastcall($account_id, '44198123456')
         *
         * Result:
         * stdClass Object
            (
                [success] =>
                [errors] => Array
                    (
                        [0] => No data for 44198123456
                    )

            )
         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $number  - Phone number on account
         * @return object
         */
        public function lastcall($account_id, $number)
        {
            // set endpoint address
            $endpoint = 'numbers/'.$account_id.'/allocated/'.$number.'/lastcall';

            print_r($endpoint);

            return $this->get(
                $this->config->baseuri.$endpoint
            );
        }

    }

}
