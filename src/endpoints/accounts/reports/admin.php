<?php
namespace opensaucesystems\simwood\endpoints\accounts\reports {

    class admin {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            $this->config = $config;
        }

        /**
         * Accounts - report - admin - cdr
         *
         * Request report of non-call related charges
         * Usage:  $simwood->accounts->reports->admin->cdr($account_id, 'day')
         * Result:
           stdClass Object
            (
                [mode] => chgd
                [date] => 2015-12-10
                [type] => chg
                [account] => 930825
                [format] => json
                [hash] => 71289698ae810a1a43b4d41f5c67004f
                [link] => /v3/files/930825/71289698ae810a1a43b4d41f5c67004f
            )

            // nothing returned!?, makes sense...
            Array
            (
            )

         *  Note: To get the above report data you must call the files endpoint:
         *  $simwood->files->fetch($report->account, $report->hash)
         *
         *
         * @param int $account_id   - The account id which is provided to you by simwood
         * @param string $action    - Options are [day]
         * @param string $date      - Optional: mySQL format datetime string
         * @return object
         */
        public function cdr($account_id, $action = 'day', $date = null)
        {
            $endpoint = 'accounts/'.$account_id.'/reports/admin/cdr/'.$action;

            $data = null;
            if (!empty($date)) {
                $data['date'] = $date;
            }

            return $this->post(
                $this->config->baseuri.$endpoint,
                $data
            );
        }

    }

}
