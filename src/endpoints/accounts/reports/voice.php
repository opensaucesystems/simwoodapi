<?php
namespace opensaucesystems\simwood\endpoints\accounts\reports {

    class voice {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            $this->config = $config;
        }

        /**
         * Accounts - report - voice - summary
         *
         * Query account for voice cdr summary data by day, in or out
         * Usage:  $simwood->accounts->reports->voice->summary($account_id, 'day', 'in')
         * Result:
            stdClass Object
            (
                [mode] => vsind
                [date] => 2015-12-10
                [type] => voice_summary
                [account] => 930000
                [format] => json
                [hash] => 69ee2fa7b94a8280a53e490c89b13123
                [link] => /v3/files/930825/69ee2fa7b94a8280a53e490c89b13123
            )
         *  Note: To get the report data you must call the files endpoint:
         *  $simwood->files->fetch($report->account, $report->hash)
         *
         * @param int $account_id   - The account id which is provided to you by simwood
         * @param string $action    - Options are [day]
         * @param string $subaction - Options are [in|out]
         * @param string $date      - Optional mysql format datetime string
         * @return object
         */
        public function summary($account_id, $action = 'day', $subaction = 'out', $date = null)
        {
            $endpoint = 'accounts/'.$account_id.'/reports/voice/summary/'.$action.'/'.$subaction;

            $data = null;
            if (!empty($date)) {
                $data['date'] = $date;
            }

            return $this->post(
                $this->config->baseuri.$endpoint,
                $data
            );
        }

        /**
         * Accounts - report - voice - cdr
         *
         * Query account for voice cdr summary data by day, in or out
         * Usage:  $report = $simwood->accounts->reports->voice->cdr($account_id, 'latest', 10)
         * Result:
            Array
            (
                [0] => stdClass Object
                    (
                        [account] => 930000
                        [reference] => 22285b6b71602935605072b3ffc083101ad23123
                        [date] => 2015-12-10
                        [time] => 13:27:00
                        [calldate] => 2015-12-10 13:27:00
                        [from] => 01234567890
                        [fromdesc] => NTS
                        [to] => 441234567890
                        [todesc] => NTS to VoIP
                        [toid] => 2523
                        [secs_call] => 2
                        [secs_billed] => 2
                        [secs_peak] => 2
                        [secs_offpeak] => 0
                        [secs_weekend] => 0
                        [chg_peak] => 0
                        [chg_offpeak] => 0
                        [chg_weekend] => 0
                        [chg_connection] => 0
                        [chg_total] => 0
                        [currency] => GBP
                        [trunk] => 930000-L001
                        [tag] =>
                    )
            )
         *  Note: To get the above report data you must call the files endpoint:
         *  $simwood->files->fetch($report->account, $report->hash)
         *
         * @param int $account_id   - The account id which is provided to you by simwood
         * @param string $action    - Options are [day|latest]
         * @param mixed $subaction  - Optional: The number of results to report on [10|100|1000|10000]
         * @param string $date      - Optional: YYY-MM-DD format date string
         * @return object
         */
        public function cdr($account_id, $action = 'day', $subaction = '', $date = null)
        {
            $endpoint = 'accounts/'.$account_id.'/reports/voice/cdr/'.$action;

            if (!empty($subaction)) {
                $endpoint .= '/'.$subaction;
            }

            $data = null;

            if (!empty($date)) {
                $data['date'] = $date;
            }

            return $this->post(
                $this->config->baseuri.$endpoint,
                $data
            );
        }
    }

}
