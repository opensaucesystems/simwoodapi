<?php
namespace opensaucesystems\simwood\endpoints\accounts {

    use opensaucesystems\simwood\endpoints\baseEndpoint;

    class reports extends baseEndpoint {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

    }

}
