<?php
namespace opensaucesystems\simwood\endpoints {

    class files extends baseEndpoint {

        use \opensaucesystems\simwood\service\http;
        use \opensaucesystems\simwood\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        /**
         * Fetch asynchronous report "files"
         *
         * Usage:  $simwood->files->fetch($account_id, $result->hash)
         *
         * @param int $account_id - The account id which is provided to you by simwood
         * @param string $hash    - Report hash
         * @return string
         */
        public function fetch($account_id, $hash = null)
        {
            $endpoint = 'files/'.$account_id;

            if (!empty($hash)) {
                $endpoint .= '/'.$hash;
                return $this->get($this->config->baseuri.$endpoint);
            } else {
                $result = $this->get($this->config->baseuri.$endpoint);

                if (!is_object($result)) {
                    return [];
                }

                $return = array();
                foreach ($result as $row) {
                    if (empty($row->name)) {
                        continue;
                    }
                    $return[] = $this->get($this->config->baseuri.$endpoint.'/'.$row->name);
                }
                return $return;
            }
        }

    }

}