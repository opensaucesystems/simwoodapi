<?php
namespace Opensaucesystems\Simwood;

use \Exception;

class SimwoodClient
{
    /**
     * @psalm-var array{baseuri: string, endpoint: null|string, key: string, options: array, user: string}
     */
	protected array $config;

	public function __construct(string $apiUser, string $apiKey, array $options = [])
	{
		$this->config = [
			'baseuri'  => 'https://api.simwood.com/v3/',
			'endpoint' => null,
			'user'     => $apiUser,
			'key'      => $apiKey,
			'options'  => $options,
		];
	}

    /**
     * Set the timeout for the HTTP request
     *
     * @param mixed $seconds
     * @return void
     * @throws Exception
     */
	public function timeoutIn($seconds): void
	{
		if (! is_int($seconds) && ! is_float($seconds)) {
			throw new Exception('Timeout must be an integer or float.');
		}

		$this->config['options']['timeout'] = $seconds;
	}

	public function __get(string $endpoint)
	{
		$class = __NAMESPACE__.'\\Endpoints\\'.ucfirst($endpoint);

		if (! class_exists($class)) {
			throw new Exception('Endpoint '.$class.', not implemented.');
		}

		$this->config['endpoint'] = $endpoint;

		return $this->$endpoint = new $class($this->config);
	}

}
