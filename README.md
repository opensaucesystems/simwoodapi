Simwood API Client - Wiki - Home
------------------
![Simwood Logo][1]

**About:**

This php [composer package][2] provides a simple object orientated way to interact with the [simwood.com][3] API.

**Author:**

@lcherone


**Setup:**

Using composer add the package to your project:

   `composer require opensaucesystems/simwoodapi`


**Initializing the client**

    <?php
    require 'vendor/autoload.php';

    /**
     * Initialize the API client class
     */
    $simwood = new opensaucesystems\simwood\APIClient(
        'APIUSER', //<-- get from simwood
        'APIKEY'   //<-- get from simwood
    );
    ?>

**Account Id:**

I've chosen to not include this in the class construct, so it must be passed to each method call if required, this is because of the way the API has multiple accounts levels.


You can set it in a variable or you can hard-code it to the method call, no biggie!

    <?php
    $account_id = '999999';
    ?>

**Making calls:**

To interact with the API its as simple as calling a class method or property, endpoints are dynamically loaded when traversed to. 

See the wikis below for further details on each endpoint.

    [ ] Todo
    [-] Partially done
    [X] Done
    [!] Broken


- [X] **Tools**
    - [X] [My IP](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Tools/My%20IP)
    - [X] [Time](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Tools/Time)

- [X] **Accounts**
    - [X] [Credit](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Credit/Invoices)
        - [X] [Invoices](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Credit/Invoices)
            - [X] [All](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Credit/Invoices)
            - [X] [Unpaid](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Credit/Invoices)
            - [X] [Paid](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Credit/Invoices)
            - [X] [PDF](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Credit/Invoices)
    - [X] **Prepay**
        - [X] [Balance](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Balance)
        - [X] [Low Balance Alerts](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Low%20Balance%20Alerts)
        - [X] [Locking](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Locking)
        - [X] [Summary](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Summary)
        - [X] [Pre-payments](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Pre-payments)
            - [X] [All](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Pre-payments)
            - [X] [Latest](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Pre-payments)
        - [X] [Transfers](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Transfers)
            - [X] [All](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Transfers)
            - [X] [Latest](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Prepay/Transfers)
    - [X] **Rates**
        - [X] [Breakouts](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Breakouts)
            - [X] [Date](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Breakouts)
            - [X] [Future](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Breakouts)
        - [X] [Codes](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Codes)
            - [X] [Date](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Codes)
            - [X] [Future](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Codes)
        - [X] [CSV](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/CSV)
        - [X] [Lookup](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Rates/Lookup)
    - [X] **Reports**
        - [X] Voice
            - [X] [Summary](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/Voice/Summary)
            - [X] [CDR](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/Voice/CDR)
                - [X] [Day](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/Voice/CDR)
                - [X] [Latest](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/Voice/CDR)
        - [X] SMS
            - [X] [CDR](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/SMS/CDR)
                - [X] [Day](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/SMS/CDR)
                - [X] [Latest](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/SMS/CDR)
        - [X] Admin
            - [X] [CDR](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/Admin/CDR)
            - [X] [Day](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Reports/Admin/CDR)
    - [X] **Summary**
        - [X] [In](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
        - [X] [Out](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
            - [X] [by destid](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
            - [X] [by iso](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
            - [X] [by codec](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
            - [X] [by tag](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
            - [X] [by trunk](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Summary)
    - [X] **Notifications**
        - [X] [Available](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Notifications)
        - [X] [Type](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Notifications)
        - [X] [Method (email, http, sms)](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Notifications)
        - [X] [Hash](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Notifications)
        - [X] [History](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Account/Notifications)

- [-] **Voice**
    - [X] [Limits](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Voice/Limits)
    - [X] [Channels](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Voice/Channels)
    - [X] [Current](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Voice/Channels)
    - [X] [History](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Voice/History)
    - [X] [In-progress](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Voice/In-progress)
    - [ ] Outbound - Trunk Management
    - [ ] Outbound - Balance
    - [ ] Outbound - In-progress
    - [ ] Outbound - ACL
    - [ ] Outbound - ACL/IP
    - [ ] Outbound - Password Reset
    - [ ] Destination ACL
    - [ ] IDA

- [X] **Inline CDR**
    - [X] [CDR](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Inline%20CDR)
        - [X] [{YYYY-MM-DD}](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Inline%20CDR)
        - [X] [{YYYY-MM-DD}/{REFERENCE}](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Inline%20CDR)

- [X] [Real-time Calls in Progress](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Voice/In-progress)

- [-] **Numbers**
    - [X] [Ranges](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Ranges)
    - [X] [Available](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Available)
    - [X] [Consecutive](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Consecutive)
    - [X] [Allocated](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Allocated)
        - [X] [All](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Allocated)
        - [X] [10|100|100|1000](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Allocated)
    - [-] Number
        - [X] [Config](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Config)
        - [ ] SMS
        - [ ] 999
        - [X] [Default Config](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Numbers/Default%20Config)
        - [X] [Manage]
            - [X] [Info](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/browse/Numbers/Manage)
            - [X] [Allocate](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/browse/Numbers/Manage)
            - [X] [Delete/Deconfigure](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/browse/Numbers/Manage)

- [ ] **Porting**
    - [ ] Ports

- [ ] **Fax**
    - [ ] Inbound
    - [ ] Hash

- [ ] **Messaging**
    - [ ] SMS
    - [ ] Fax

- [X] [Asynchronous Report File](https://bitbucket.org/opensaucesystems/simwoodapi/wiki/Files)


  [1]: http://i.stack.imgur.com/6RB4O.png
  [2]: https://packagist.org/packages/opensaucesystems/simwoodapi
  [3]: https://www.simwood.com/
  [4]: http://mirror.simwood.com/pdfs/APIv3.pdf