# Simwood API Client

![Simwood Logo][ico-simwood]

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Total Downloads][ico-downloads]][link-downloads]

This php [composer package][link-packagist] provides a simple object orientated way to interact with the [simwood.com][link-simwood] API.

## Install

Via Composer

``` bash
composer require opensaucesystems/lxd
```

## Usage

Initialize the API client class:

```php
$simwood = new \Opensaucesystems\Simwood\SimwoodClient(
    'APIUSER', //<-- get from simwood
    'APIKEY'   //<-- get from simwood
);
```

I've chosen to not include the `$account_id` in the class construct, so it must be passed to each method call if required, this is because of the way the API has multiple accounts levels.

To interact with the API its as simple as calling a class method or property, endpoints are dynamically loaded when traversed to.

## Docs

See the docs below for further details on each endpoint.

    [ ] Todo
    [-] Partially done
    [X] Done
    [!] Broken


- [X] **Tools**
    - [X] [My IP](./docs/Tools/My%20IP.md)
    - [X] [Time](./docs/Tools/Time.md)

- [X] **Accounts**
    - [X] **Credit**
        - [X] [Invoices](./docs/Account/Credit/Invoices.md)
            - [X] **All**
            - [X] **Unpaid**
            - [X] **Paid**
            - [X] **PDF**
    - [X] **Prepay**
        - [X] [Balance](./docs/Account/Prepay/Balance.md)
        - [X] [Low Balance Alerts](./docs/Account/Prepay/Low%20Balance%20Alerts.md)
        - [X] [Locking](./docs/Account/Prepay/Locking.md)
        - [X] [Summary](./docs/Account/Prepay/Summary.md)
        - [X] [Pre-payments](./docs/Account/Prepay/Pre-payments.md)
            - [X] **All**
            - [X] **Latest**
        - [X] [Transfers](./docs/Account/Prepay/Transfers.md)
            - [X] **All**
            - [X] **Latest**
    - [X] **Rates**
        - [X] [Breakouts](./docs/Account/Rates/Breakouts.md)
            - [X] **Date**
            - [X] **Future**
        - [X] [Codes](./docs/Account/Rates/Codes.md)
            - [X] **Date**
            - [X] **Future**
        - [X] [CSV](./docs/Account/Rates/CSV.md)
        - [X] [Lookup](./docs/Account/Rates/Lookup.md)
    - [X] **Reports**
        - [X] Voice
            - [X] [Summary](./docs/Account/Reports/Voice/Summary.md)
            - [X] [CDR](./docs/Account/Reports/Voice/CDR.md)
                - [X] **Day**
                - [X] **Latest**
        - [X] SMS
            - [X] [CDR](./docs/Account/Reports/SMS/CDR.md)
                - [X] **Day**
                - [X] **Latest**
        - [X] Admin
            - [X] [CDR](./docs/Account/Reports/Admin/CDR.md)
                - [X] **Day**
    - [X] [Summary](./docs/Account/Summary.md)
        - [X] **In**
        - [X] **Out**
            - [X] **by destid**
            - [X] **by iso**
            - [X] **by codec**
            - [X] **by tag**
            - [X] **by trunk**
    - [X] [Notifications](./docs/Account/Notifications.md)
        - [X] **Available**
        - [X] **Type**
        - [X] **Method**
        - [X] **Hash**
        - [X] **History**

- [-] **Voice**
    - [X] [Limits](./docs/Voice/Limits.md)
    - [X] [Channels](./docs/Voice/Channels.md)
    - [X] [Current](./docs/Voice/Channels.md)
    - [X] [History](./docs/Voice/History.md)
    - [X] [In-progress](./docs/Voice/In-progress.md)
    - [ ] Outbound - Trunk Management
    - [ ] Outbound - Balance
    - [ ] Outbound - In-progress
    - [ ] Outbound - ACL
    - [ ] Outbound - ACL/IP
    - [ ] Outbound - Password Reset
    - [ ] Destination ACL
    - [ ] IDA

- [X] [Inline CDR](./docs/Inline%20CDR.md)
    - [X] **CDR**
        - [X] **{YYYY-MM-DD}**
        - [X] **{YYYY-MM-DD}/{REFERENCE}**

- [-] **Numbers**
    - [X] [Ranges](./docs/Numbers/Ranges.md)
    - [X] [Available](./docs/Numbers/Available.md)
    - [X] [Consecutive](./docs/Numbers/Consecutive.md)
    - [X] [Allocated](./docs/Numbers/Allocated.md)
        - [X] **All**
        - [X] **10|100|100|1000**
    - [-] Number
        - [X] [Config](./docs/Numbers/Number/Config.md)
        - [ ] SMS
        - [X] [999](./docs/Numbers/Number/999.md)
        - [X] [Default Config](./docs/Numbers/Number/Default%20Config.md)
        - [X] [Manage](./docs/browse/Numbers/Number/Manage.md)
            - [X] **Info**
            - [X] **Allocate**
            - [X] **Delete/Deconfigure**

- [ ] **Porting**
    - [ ] Ports

- [ ] **Fax**
    - [ ] Inbound
    - [ ] Hash

- [ ] **Messaging**
    - [ ] SMS
    - [ ] Fax

- [X] [Asynchronous Report File](./docs/Files.md)

[ico-simwood]: http://i.stack.imgur.com/6RB4O.png
[ico-version]: https://img.shields.io/packagist/v/opensaucesystems/simwoodapi.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/opensaucesystems/simwoodapi.svg?style=flat-square

[link-simwood]: https://www.simwood.com/
[link-packagist]: https://packagist.org/packages/opensaucesystems/simwoodapi
[link-downloads]: https://packagist.org/packages/opensaucesystems/simwoodapi
[link-author]: https://opensauce.systems
[link-contributors]: ../../contributors
