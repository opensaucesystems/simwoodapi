<?php

use Opensaucesystems\Simwood\SimwoodClient;

beforeEach(function () {
    $this->simwood = new SimwoodClient('test_user', 'test_apikey');
});

it('can get my ip', function () {
    $response = $this->simwood->tools->myip();

    expect($response)->toBeObject();
    expect($response->ip)->toMatch('/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/');
});

it('can get the time', function () {
    $response = $this->simwood->tools->time();

    expect($response)->toBeObject();
    expect($response->timestamp)->toBeInt();
    expect($response->rfc)->toMatch('/[\w]{3,4},\s[0-9]{2}\s[\w]{3,4}\s20[2-9][0-9]\s[0-2][0-9]:[0-5][0-9]:[0-5][0-9]\s\+[0-9]{4}/');
    expect($response->api)->toMatch('/20[2-9][0-9]-[0-1][0-9]-[0-3][0-9]\s[0-2][0-9]:[0-5][0-9]:[0-5][0-9]/');
});
