[Back to index](/#docs)

**IP Address**

Your IP address, as seen by the Simwood API service

```php
$simwood->tools->myip;
```

**Result:**

```
stdClass Object
(
    [ip] => 123.123.123.123
)
```
