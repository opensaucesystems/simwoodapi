[Back to index](/#docs)

# API Tools #

The following tools are made available without authentication to help integrate with the Simwood API.

**Time**

The current server timestamp

```php
$simwood->tools->time;
```

**Result:**

```
stdClass Object
(
    [timestamp] => 1449690039
    [rfc] => Wed, 09 Dec 2015 19:40:39 +0000
    [api] => 2015-12-09 19:40:39
)
```
