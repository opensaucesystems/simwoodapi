[Back to index](/#docs)

## Voice - Channels


Current channel utilisation.

NB: this is returned as an array (with one member) for compatibility with the /channels/history function detailed below

```
php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @return object
 */

$simwood->voice->channels->current($account_id);
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [datetime] => 2015-12-12 23:23:23
            [channels] => 0
            [channels_out] => 0
            [channels_in] => 0
        )
)
```
