[Back to index](/#docs)

## Voice - In-Progress

Number and value of calls in progress relative to account balance. Useful for fraud monitoring.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @return object
 */

$simwood->voice->inprogress($account_id);
```

**Result:**

```
stdClass Object
(
    [datetime] => 2015-12-10 16:51:00
    [total] => 0
    [callcount] => 0
    [balance] => 2
    [currency] => GBP
    [balance_locked] => 0
    [balance_available] => 2
    [calls] => Array
    (
    )
    [countries] => Array
    (
    )

)
```
