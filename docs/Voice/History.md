[Back to index](/#docs)

## Voice - CDR - History

Recent (around 24h) channel utilisation samples,
The channels count shows the peak number of channels in use between the previous datetime timestamp and the current one.

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param int $interval   - Optional interval for samples in the following form [1m, 5m, 1h]
 * @return array
 */

$simwood->voice->channels->history($account_id, '1h');
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [datetime] => 2015-12-12 23:34:27
            [channels] => 0
        )

)
```
