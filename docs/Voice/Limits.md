[Back to index](/#docs)

## Voice - Limits

Voice - Account Limits (and Dynamic Channel Limits)

**Dynamic Channel Limits**
Any dynamic channel limits shown in the "dynamic" block take precedence
over the usual account limits. e.g. in the above example due to the
low balance (£15) there is a concurrent channel limit of 3 channels
and a rate limit of 3 calls per second.

**Changing Channel / Rate Limits**
These limits are set per-account by Simwood and are based on your
traffic. We do not charge for channels and impose these limits only
to manage our network utilisation.  If you require more channels or
a higher rate of calls per second then please contact us via our
support ticket system.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @return object
 */

$simwood->voice->limits($account_id);
```

**Result:**

```
stdClass Object
(
    [channel_allocation] => 30
    [limit_concurrent_in] =>
    [limit_concurrent_out] => 30
    [limit_concurrent_out_per_number] =>
    [limit_concurrent_out_international] =>
    [limit_concurrent_out_hotspot] =>
    [limit_rate_out] => 30/10s
    [limit_rate_out_international] =>
    [limit_rate_out_hotspot] =>
    [dynamic] => stdClass Object
        (
            [balance] => 2
            [limit_concurrent_out] => 3
            [limit_rate_out] => 3/1s
        )

)
```
