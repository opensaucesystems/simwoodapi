[Back to index](/#docs)

## Accounts - Files (Asynchronous Reports)

Fetch asynchronous report "files"

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $hash    - Report hash
 * @return mixed
 */

$simwood->files->fetch($account_id, $result->hash)
```
