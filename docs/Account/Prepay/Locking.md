[Back to index](/#docs)

## Accounts - Prepay - Balance lock

Query account for balance lock value.

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are alert, locked
 * @return array
 */

$simwood->accounts->prepay->balance($account_id, 'locked')
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [account] => 930000
            [balance] => 2
            [locked] => 0
            [available] => 2
        )

)
```

## Account - Prepay - Balance lock - Update

Update account locked value.

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are alert, locked
 * @param array $data     - Alert balance lock value
 * @return array
 */

$simwood->accounts->prepay->balance($account_id, 'locked', ['balance' => 123])
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [account] => 930000
            [balance] => 2
            [locked] => 123
            [available] => 2
        )

)
```

## Account - Prepay - Balance lock - Delete

Delete account locked value (passing 0 as the value to set, which will remove lock)

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are alert, locked
 * @param array $data     - Alert balance lock value
 * @return array
 */

$simwood->accounts->prepay->balance($account_id, 'locked', ['balance' => 0])
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [account] => 930000
            [balance] => 2
            [locked] => 0
            [available] => 2
        )

)
```
