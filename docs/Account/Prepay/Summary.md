[Back to index](/#docs)

## Accounts - Prepay - Summary

Query for prepay summary report.

```php
<?php
/**
 * @param int $account_id  - The account id which is provided to you by simwood
 * @param string $dateFrom - Optional date from (format: YYYY-MM-DD)
 * @param string $dateTo   - Optional date to   (format: YYYY-MM-DD)
 * @return array
 */

$simwood->accounts->prepay->summary($account_id);
```

**Result:**

```
stdClass Object
(
    [mode] =>
    [from] => 2015-12-10 00:00:00
    [to] => 2015-12-10 23:59:59
    [type] => balance_summary
    [account] => 930000
    [format] => json
    [hash] => b07f7b1cff3c187d8df79fc9fe18aabc
    [link] => /v3/files/930825/b07f7b1cff3c187d8df79fc9fe18aabc
)
```

**Note:**

To get the report data you must then poll the files endpoint to collect the report:

    $simwood->files->fetch($report->account, $report->hash)
