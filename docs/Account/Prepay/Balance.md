[Back to index](/#docs)

## Accounts - Prepay - Balance

Query account for balance alert value.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @return array
 */

$simwood->accounts->prepay->balance($account_id);
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [balance] => 2.00000
            [currency] => GBP
        )

)
```
