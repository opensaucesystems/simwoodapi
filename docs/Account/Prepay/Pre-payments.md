[Back to index](/#docs)

## Accounts - Prepay - Prepayments

Query for prepayments on account.

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are all, latest
 * @param int $subaction  - The number of prepayments to return
 * @return array
 */

$simwood->accounts->prepay->prepayments($account_id);
$simwood->accounts->prepay->prepayments($account_id, 'all');
$simwood->accounts->prepay->prepayments($account_id, 'latest', 10);

```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [date] => 2015-11-12
            [currency] => GBP
            [net] => 2.00
            [VAT] => 0.00
            [total] => 2.00
            [applied_to_account] => 2.00
            [invoice_reference] => I3010000000
        )

)
```
