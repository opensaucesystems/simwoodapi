[Back to index](/#docs)

## Accounts - Prepay - Balance Alerts

Query account for balance alert value.

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are alert, locked
 * @return array
 */

$simwood->accounts->prepay->balance($account_id, 'alert')
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [account] => 930000
            [alert_balance] => 0
        )

)
```

## Account - Prepay - Balance Alerts - Update

Update account balance alert

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are alert, locked
 * @param array $data     - Alert balance value
 * @return array
 */

$simwood->accounts->prepay->balance($account_id, 'alert', ['alert_balance' => 123])
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [account] => 930000
            [alert_balance] => 123
        )

)
```


## Account - Prepay - Balance Alerts - Remove set alert value

Remove account balance alert

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are alert, locked
 * @param array $data     - Alert balance value
 * @return array
 */

$simwood->accounts->prepay->balance($account_id, 'alert', [])
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [account] => 930000
            [alert_balance] => 0
        )

)
```
