[Back to index](/#docs)

## Accounts - Notifications

Get and set account notifications.

NB: Think I've packed to much into this method, so it may get changed in future.

### List active notifications on your account:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @return object
 */

$simwood->accounts->notifications($account_id)
```

### List available notification TYPE's:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @return object
 */

$simwood->accounts->notifications($account_id, 'available')
```

### List configured recipients for notifications of {TYPE}:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @param string $method  - Type of notification method [email, http, sms]
 * @return object
 */

$simwood->accounts->notifications($account_id, 'blocked_calls', 'get')
```

### Delete all configured notifications of {TYPE}:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @param string $method  - Type of notification method [email, http, sms]
 * @return object
 */

$simwood->accounts->notifications($account_id, 'blocked_calls', 'delete')
```

### Shows all configured recipients of notifications of {TYPE} using {METHOD}:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @param string $method  - Type of notification method [email, http, sms]
 * @return object
 */

$simwood->accounts->notifications($account_id, 'blocked_calls', 'email')
```

### Add a new notification recipient to receive notifications of {TYPE} using {METHOD}:

Returns a hash corresponding to this recipient

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @param string $method  - Type of notification method [email, http, sms]
 * @param string $hash    - The {HASH} referred to above can be generated
 *                          locally and is simply an md5()’d version of the notification address
 * @return object
 */

$simwood->accounts->notifications($account_id, 'blocked_calls', 'email', 'new destination')
```

### Shows the information on this recipient:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @param string $method  - Type of notification method [email, http, sms]
 * @param string $hash    - The {HASH} referred to above can be generated
 *                          locally and is simply an md5()’d version of the notification address
 * @param string $do      - Options are get or delete
 * @return object
 */

$simwood->accounts->notifications($account_id, 'blocked_calls', 'email', '{HASH}', 'get')
```

### Deletes this recipient:

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Type of active notification on your account [blocked_calls, prepay_debit, ...]
 * @param string $method  - Type of notification method [email, http, sms]
 * @param string $hash    - The {HASH} referred to above can be generated
 *                          locally and is simply an md5()’d version of the notification address
 * @param string $do      - Options are get or delete
 * @return object
 */

$simwood->accounts->notifications($account_id, 'blocked_calls', 'email', '{HASH}', 'delete')
```

### History

```
*  Retrieve a history of recent (last 60 days) notifications on your account
*   All parameters below are optional, by default will return all notifications for the last 60 days:
*   class:      Class of notification ( e.g. trunk or billing )
*   date_start: Start date (no more than 60 days ago)
*   date_end:   End date (YYYY-MM-DD H:i:s)
*
*  Examples:
*   $simwood->accounts->notifications($account_id, 'history')
*   $simwood->accounts->notifications($account_id, 'history', 'class')
*   $simwood->accounts->notifications($account_id, 'history', 'class', 'date_start', 'date_end')
```
