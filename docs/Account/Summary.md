[Back to index](/#docs)

## Accounts - Summary [BETA]

The new Summary Reports are not asynchronous, the response is inline.

**NB These are intended to be indicative only and are not, at this time, suitable for billing purposes**

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @param string $key       - destid|iso|codec|tag|trunk
 * @param string $direction - in|out
 * @param array $data       - Report filtering array
 * @return array
 */

$simwood->accounts->summary($account_id, 'destid|iso|codec|tag|trunk', 'in|out', [
    'date_start' => 'YYYY-MM-DD H:i:s',
    'date_end' => 'YYYY-MM-DD H:i:s',
    'limit' => 9999,
    'sort' => 'trunk',
    'filter' => array('search_key' => 'value'),
]);
```

**Result: `$simwood->accounts->summary($account_id, 'trunk')`**

```
Array
(
    [0] => stdClass Object
        (
            [trunk] => 930000-l001
            [calls] => 2
            [acd] => 0.08
            [minutes] => 0.17
            [charges] => 0
        )

)
```
