[Back to index](/#docs)

## Accounts - Credit

Fetch an accounts credit invoice data.

```php
<?php
/**
 * @param int    $account_id - The account id which is provided to you by simwood
 * @param string $action     - Default is invoice but the API may add others
 * @param string $subaction  - Options are all, unpaid and paid.
 * @return object
 */

 $records = $simwood->accounts->credit($account_id, 'invoices', 'all');
```

**Result:**

```
stdClass Object
(
    [Invoices] => Array
    (
        [0] => stdClass Object
        (
            [InvoiceNumber] => I3010008404
            [Date] => 2015-11-12
            [DueDate] => 2015-11-12
            [Currency] => GBP
            [Net] => 2.00
            [VAT] => 0.00
            [Total] => 2.00
            [AmountPaid] => 2.00
            [AmountCredited] => 0.00
            [AmountDue] => 0.00
        )

    )

    [TotalDue] => stdClass Object
    (
        [GBP] => 0
    )

    [TotalOverDue] =>
)
```

## Accounts - Credit - Invoice

Generate an account invoice PDF string or view in browser.

```php
<?php
/**
 * @param int    $account_id    - The account id which is provided to you by simwood
 * @param string $InvoiceNumber - @see $this->credit() response
 * @param bool   $sent          - Add HTTP headers for view in browser
 * @return mixed
 */

 $invoice = $simwood->accounts->invoice($account_id, 'I3000000000', true);
```
