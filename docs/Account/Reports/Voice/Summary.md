[Back to index](/#docs)

## Accounts - Report - Voice - Summary

Query account for voice cdr summary data by day, in or out.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @param string $action    - Options are [day]
 * @param string $subaction - Options are [in|out]
 * @param string $date      - Optional mysql format datetime string
 * @return object
 */

$report = $simwood->accounts->reports->voice->summary($account_id, 'day', 'in')
```

**Result:**

```
stdClass Object
(
    [mode] => vsind
    [date] => 2015-12-10
    [type] => voice_summary
    [account] => 930000
    [format] => json
    [hash] => 69ee2fa7b94a8280a53e490c89b13123
    [link] => /v3/files/930825/69ee2fa7b94a8280a53e490c89b13123
)
```

**Note:** To get the report data you must call the files endpoint:

```php
$simwood->files->fetch($report->account, $report->hash)
```
