[Back to index](/#docs)

## Accounts - Report - Admin - CDR


Request report of non-call related charges.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @param string $action    - Options are [day]
 * @param string $date      - Optional: mySQL format datetime string
 * @return object
 */

$simwood->accounts->reports->admin->cdr($account_id, 'day');
```

**Result:**

```
stdClass Object
(
    [mode] => chgd
    [date] => 2015-12-10
    [type] => chg
    [account] => 930825
    [format] => json
    [hash] => 71289698ae810a1a43b4d41f5c67004f
    [link] => /v3/files/930825/71289698ae810a1a43b4d41f5c67004f
)
```

**Note:** To get the above report data you must call the files endpoint:

```php
$simwood->files->fetch($report->account, $report->hash)
```
