[Back to index](/#docs)

## Accounts - Reports - SMS

Query account for sms cdr summary data by day, in or out.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @param string $action    - Options are [day|latest]
 * @param mixed $subaction  - Optional: The number of results to report on [10|100|1000|10000]
 * @param string $date      - Optional: YYYY-MM-DD format date string
 * @return array
 */

$report = $simwood->accounts->reports->sms->cdr($account_id, 'latest', 10);
```

**Result:**

```
To follow...
```

**Note:**

To get the above report data you must call the files endpoint:

```php
$simwood->files->fetch($report->account, $report->hash)
```
