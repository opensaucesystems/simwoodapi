[Back to index](/#docs)

## Accounts - Rates - Destination Lookup

Returns the cost of calling the specified number in your account currency.
Each rate array is keyed by the relevant deck (1 - platinum, 2 - gold, 3 silver)
and shows (p)eak, (o)ffpeak, (w)eekend rates and the (c)onnection charge

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $number  - Phone neumber to lookup, must be international format
 * @return object
 */

$simwood->accounts->rate($account_id, '441983123456');
#or via the rates method
$simwood->accounts->rates->lookup($account_id, '441983123456');
```

**Result:**

```
stdClass Object
(
    [desc] => UK - Fixed
    [account_type] => legacy
    [currency] => GBP
    [rates] => stdClass Object
        (
            [1] => stdClass Object
                (
                    [p] => 0.00249
                    [o] => 0.00249
                    [w] => 0.00249
                    [c] => 0.0000
                    [min] => 1
                    [inc] => 1
                    [ISO] => GB
                )

            [2] => stdClass Object
                (
                    [p] => 0.00249
                    [o] => 0.00249
                    [w] => 0.00249
                    [c] => 0.0000
                    [min] => 1
                    [inc] => 1
                    [ISO] => GB
                )

        )

)
```
