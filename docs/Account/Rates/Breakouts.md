[Back to index](/#docs)

## Accounts - Rates


Request a report of current account termination rates (breakouts)

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are [date, future]
 * @param string $date    - Optionally specify date in YYYY-MM-DD format to
 *                          request report of historic account termination rates
 * @return object
 */

$report = $simwood->accounts->rates->breakouts($account_id, 'date', '2015-12-15');
```

**Result:**

```
stdClass Object
(
    [mode] => bod
    [date] => 2015-12-11
    [type] => rates
    [account] => 930000
    [format] => json
    [hash] => 6668124194c8120cfffee195be2d8000
    [link] => /v3/files/930825/6668124194c8120cfffee195be2d8000
)
```

**Note:**

To get the report data you must then poll the files endpoint to collect the report:

```php
$simwood->files->fetch($report->account, $report->hash)
```

```
Array
(
    [0] => stdClass Object
        (
            [id] => 1
            [location_name] => Afghanistan - Proper
            [tariff] => 7
            [peak_charge] => 0.14080
            [offpeak_charge] => 0.14080
            [weekend_charge] => 0.14080
            [connection_charge] => 0.0000
            [basis_minimum] => 1
            [basis_increment] => 1
            [valid_from] => 2015-11-18 00:00:00
            [valid_to] => 2015-12-15 23:59:59
            [currency] => GBP
            [Type] => Premium
            [in_country_order] =>
        )

    [1] => stdClass Object
        (
            [id] => 3
            [location_name] => Albania - Tirane
            [tariff] => 7
            [peak_charge] => 0.11630
            [offpeak_charge] => 0.11630
            [weekend_charge] => 0.11630
            [connection_charge] => 0.0000
            [basis_minimum] => 1
            [basis_increment] => 1
            [valid_from] => 2015-10-21 00:00:00
              [valid_to] => 2015-12-15 23:59:59
            [currency] => GBP
            [Type] => Premium
            [in_country_order] => 100
        )
    ...
)
```
