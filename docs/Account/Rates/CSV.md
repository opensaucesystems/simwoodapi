[Back to index](/#docs)

## Accounts - Rates - CSV

Request the latest ratecard in CSV format.

NB: The options silver, platinum and gold are only for ‘legacy’ or ‘startup’ account types with multiple rate decks.
Virtual Interconnect and Managed Interconnect customers should use default.

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $type    - Options are [default|silver|platinum|gold]
 * @return object
 */

$simwood->accounts->rates->csv($account_id, 'gold');
```

**Result:**

```
"prefix","location","day","evening","weekend","connection","minimum_sec","increment_sec"
441,"UK - Fixed",0.00249,0.00249,0.00249,0.0000,1,1
442,"UK - Fixed",0.00249,0.00249,0.00249,0.0000,1,1
4474178,"UK - Mobile - Simwood",0.00680,0.00680,0.00680,0.0000,1,1
...
```
