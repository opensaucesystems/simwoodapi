[Back to index](/#docs)

## Accounts - Codes

Request a report of current termination codes (codes)

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $action  - Options are [date, future]
 * @param string $date    - Optionally specify date in YYYY-MM-DD format to
 *                          request report of historic account termination rates
 * @return object
 */

$simwood->accounts->rates->codes($account_id, 'date', '2015-12-15');
```

**Result:**

```
stdClass Object
(
    [mode] => cdf
    [type] => rates
    [account] => 930000
    [format] => json
    [hash] => ad9f9f39758bf8fc42042eab2c479800
    [link] => /v3/files/930825/ad9f9f39758bf8fc42042eab2c479800
)
```

**Note:**

To get the report data you must call the files endpoint:

```php
$simwood->files->fetch($report->account, $report->hash)
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [Prefix] => 40787
            [id] => 3073
            [valid_from] => 2015-12-16 00:00:00
            [valid_to] => 0000-00-00 00:00:00
        )

    [1] => stdClass Object
        (
            [Prefix] => 40787
            [id] => 3076
            [valid_from] => 2015-09-01 00:00:00
            [valid_to] => 2015-12-15 23:59:59
        )
    ...
)
```
