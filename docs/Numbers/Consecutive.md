[Back to index](/#docs)

## Numbers - Consecutive

Request a report of 10,20,30,40,50,60,70,80,90 or 100 consecutive numbers available
for allocation matching the pattern specified.

```php
<?php
/**
 * @param int     $account_id - The account id which is provided to you by simwood
 * @param string  $pattern    - Pattern to match * wildcard
 * @param string  $type       - Premium type options are [all|gold|standard]
 * @param string  $count      - 1,10 or 100 numbers available for allocation matching the pattern specified.
 * @return array
 */

$simwood->numbers->consecutive($account_id, '1983*');
```

**Result:**

```
stdClass Object
(
    [mode] => 10
    [pattern] => 1983*
    [type] => csec
    [account] => 930000
    [format] => json
    [hash] => 3bf98fedaf1f0fbcea4e305b6e084333
    [link] => /v3/files/930825/3bf98fedaf1f0fbcea4e305b6e084333
)
```

*Note:*

To get the report data you must call the files endpoint:

    $simwood->files->fetch($report->account, $report->hash);

```
Array
(
    [0] => stdClass Object
        (
            [country_code] => 44
            [start] => 1983632000
            [end] => 1983632009
            [recommended_gold_premium] => 210
            [wholesale_gold_premium] => 105.0000
            [block] => 894a404aa15e7e7353243e31982819ed
            [bill_class] => DDI
            [SMS] => 0
        )

    [1] => stdClass Object
        (
            [country_code] => 44
            [start] => 1983632020
            [end] => 1983632029
            [recommended_gold_premium] => 1085
            [wholesale_gold_premium] => 542.5000
            [block] => 894a404aa15e7e7353243e31982819ed
            [bill_class] => DDI
            [SMS] => 0
        )
    ...
)
```
