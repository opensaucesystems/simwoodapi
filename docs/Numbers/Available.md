[Back to index](/#docs)

## Numbers - Available

Returns 1,10 or 100 numbers available for allocation matching the pattern specified.

One of all, gold, or standard should be specified in the URL;

 - all returns all available numbers matching pattern.
 - gold returns only gold numbers matching pattern.
 - standard returns only non-gold numbers matching pattern.

```php
<?php
/**
 * @param int     $account_id - The account id which is provided to you by simwood
 * @param string  $pattern    - Pattern to match * wildcard
 * @param string  $type       - Premium type options are [all|gold|standard]
 * @param string  $count      - 1,10 or 100 numbers available for allocation matching the pattern specified.
 * @return array
 */

$simwood->numbers->available($account_id, '1983*');
```

**Result:**

```
Array
(
    [0] => stdClass Object
        (
            [country_code] => 44
            [number] => 1983632222
            [recommended_gold_premium] => 1550
            [wholesale_gold_premium] => 775
            [block] => 894a404aa15e7e7353243e31982819ed
            [bill_class] => DDI
            [SMS] => 0
        )

    [1] => stdClass Object
        (
            [country_code] => 44
            [number] => 1983632632
            [recommended_gold_premium] => 1000
            [wholesale_gold_premium] => 500
            [block] => 894a404aa15e7e7353243e31982819ed
            [bill_class] => DDI
            [SMS] => 0
        )
     ...
)
```
