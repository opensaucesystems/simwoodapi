[Back to index](/#docs)

## Numbers - Allocated

Request a report of all current allocated numbers on account.

count  : Request a report of the first [ 10 | 100 | 1,000 | 10,000 ]
         numbers that match the optional pattern.

pattern: Optionally specify to include only those numbers that match
         the specified pattern. (can use wildcards e.g *203*).

key:     Only return those numbers that match the specified key
         in their metadata (see Advanced Routing below)
         NB Keys are case-insensitive, wildcards not supported.


```php
<?php
/**
 * @param int    $account_id - The account id which is provided to you by simwood
 * @param array  $data       - Configuration request array
 * @return array
 */

$report = $simwood->numbers->allocated($account_id, [
    'count' => 10,
    'pattern' => '1983*',
    'key' => null,
]);
```

**Result:**

```
stdClass Object
(
    [quantity] => 10
    [mode] => nlsn
    [pattern] => 1983*
    [type] => nlist
    [account] => 930000
    [format] => json
    [hash] => 072dfa11ceded69978711888030f8000
    [link] => /v3/files/930825/072dfa11ceded69978711888030f8000
)
```

*Note:*

To get the report data you must call the files endpoint:

    $simwood->files->fetch($report->account, $report->hash);

```
Array
(
    [0] => stdClass Object
        (
            [country_code] => 44
            [number] => 1983632140
            [gold_price] => 0
            [block] => 894a404aa15e7e7353243e31982819ed
            [type] => DDI
            [SMS] => 0
        )

)
```
