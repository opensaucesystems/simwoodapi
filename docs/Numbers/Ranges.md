[Back to index](/#docs)

## Numbers - Ranges

Retrieves a list of all available number ranges, including descriptions.
This is intended for customers to populate, for example, a drop down to allow customers to select an area.

```php
<?php
/**
 * @param int $account_id   - The account id which is provided to you by simwood
 * @return object
 */

$simwood->numbers->ranges($account_id);
```

**Result:**

```
stdClass Object
(
    [success] => 1
    [data] => Array
        (
            [0] => stdClass Object
                (
                    [id] => 03dd542cafcecf43fc06024ee6099311424c71cf
                    [prefix] => 113403
                    [sabc] => 1134
                    [description] => Geographic - Leeds
                    [chargeband] => geo
                )

            [1] => stdClass Object
                (
                    [id] => c36965d451c1fa708b1c84332af9f52094a16c48
                    [prefix] => 114312
                    [sabc] => 1143
                    [description] => Geographic - Sheffield
                    [chargeband] => geo
                )
            ...
        )
)
```
