[Back to index](/#docs)

## Numbers - Manage

The new Summary Reports are not asynchronous, the response is inline.

**NB These are intended to be indicative only and are not, at this time, suitable for billing purposes**

```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $number  - Phone number on account
 * @return array
 */

$simwood->numbers->manage->info($account_id, '44198123456')
```

**Result:**

```
stdClass Object
(
    [basic] => stdClass Object
        (
            [link] => /v3/numbers/930000/allocated/44198123456/basic
            [country_code] => 44
            [number] => 1983123456
            [recommended_gold_premium] =>
            [wholesale_gold_premium] => 0.0000
            [block] => 894a404aa15e7e7353243e3198281333
            [bill_class] => DDI
            [SMS] => 0
        )

    [voice] => stdClass Object
        (
            [link] => /v3/numbers/930000/allocated/44198123456/voice
            [app] => Custom
            [appdata] => Advanced Routing
        )

    [fax] => stdClass Object
        (
            [link] => /v3/numbers/930000/allocated/44198123456/fax
        )

    [sms] => stdClass Object
        (
            [link] => /v3/numbers/930000/allocated/44198123456/sms
        )

    [999] => stdClass Object
        (
            [link] => /v3/numbers/930000/allocated/44198123456/999
        )

)
```

## Numbers - Manage - Allocate number

Allocate an available number to the account.


```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $number  - Phone number on account
 * @return array
 */

$simwood->numbers->manage->allocate($account_id, '44198123456')
```

**Result:**

```
...
```

## Numbers - Manage - De-configure

De-configure and irrevocably remove number from account


```php
<?php
/**
 * @param int $account_id - The account id which is provided to you by simwood
 * @param string $number  - Phone number on account
 * @return array
 */

$simwood->numbers->manage->remove($account_id, '44198123456')
//or
$simwood->numbers->manage->delete($account_id, '44198123456')
//or
$simwood->numbers->manage->deallocate($account_id, '44198123456')
```

**Result:**

```
...
```
