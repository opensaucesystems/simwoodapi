[Back to index](/#docs)

## Default Number Routing Configuration - Advanced

Set a default configuration which will be used for all numbers on your account where no other
configuration exists - this is ideal for customers who send all calls
to a SIP URI and handle onward routing themselves


```php
<?php
/**
 * @param int    $account_id - The account id which is provided to you by simwood
 * @param string $number     - Number to configure
 * @param array  $data       - Data for replacing active configuration for number.
 *                             This must be an array not json, see official docs
 *                             for structure and options.
 * @return array
 */
$simwood->numbers->defaults($account_id [, config array]);

```

**Result:**

```
stdClass Object
(
    [rules] => Array
        (
        )

    [routing] => stdClass Object
        (
            [default] => Array
                (
                    [0] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [type] => sip
                                    [endpoint] => 0198123456@voip.example.com
                                )

                        )

                )

        )

    [options] => stdClass Object
        (
            [enabled] => 1
            [block_payphone] => 1
            [acr] =>
            [icr] =>
        )

    [meta] => stdClass Object
        (
            [key] => value
        )

)
```
