[Back to index](/#docs)

## Numbers - Routing Configuration - Advanced

Introduced in May 2014, the following is the preferred way of
configuring a number, the previous method is still detailed in this
document but is deprecated and will be removed in a future revision
of the API. When a new configuration is provided this will take
precedence over any existing configuration.


```php
<?php
/**
 * @param int    $account_id - The account id which is provided to you by simwood
 * @param string $number     - Number to configure
 * @param array  $data       - Data for replacing active configuration for number.
 *                             This must be an array not json, see official docs
 *                             for structure and options.
 * @return array
 */
#get config
$simwood->numbers->config($account_id, '44198123456');

#set config
$simwood->numbers->config($account_id, '44198123456', [
    ...
    ... //see official docs
    ...
]);

```

**Result:**

```
stdClass Object
(
    [rules] => Array
        (
        )

    [routing] => stdClass Object
        (
            [default] => Array
                (
                    [0] => Array
                        (
                            [0] => stdClass Object
                                (
                                    [type] => sip
                                    [endpoint] => 0198123456@voip.example.com
                                )

                        )

                )

        )

    [options] => stdClass Object
        (
            [enabled] => 1
            [block_payphone] => 1
            [acr] =>
            [icr] =>
        )

    [meta] => stdClass Object
        (
            [key] => value
        )

)
```
